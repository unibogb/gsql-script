grammar nsl;

nsl  : (expr ';')* expr
     ;

expr : '(' expr ')'                          #paren
     | expr '+' expr                         #add
     | expr '-' expr                        #sub
     | expr '/' expr                        #div
     | expr '*' expr                        #mult
     | expr '++' expr                       #concat
     | expr '@' expr                       #append
     | expr '&&' expr                        #and
     | expr '||' expr                        #or
     | expr '=='    expr                    #eq
     | expr '!='    expr                    #neq
     | expr '<='    expr                    #leq
     | expr '>='    expr                    #geq
     | expr '>'     expr                    #gt
     | expr '<'     expr                    #lt
     | expr ':='    expr                    #assign
     | expr '.'     expr                    #invoke
     | 'eval(' expr ')'                      #eval
     | '(' expr  expr ')'                #apply
     | 'var(' expr ')'                       #var
     | 'not' expr                            #not
     | expr '=>' expr                        #imply
     | 'if' expr 'then' expr 'else' expr     #ifte
     | 'substring(' expr ',' expr ',' expr ')' #substring
     | expr '[' expr ']'                    #at
     | expr '[' expr ']:=' expr             #put
     | expr 'in' expr                       #contains
     | 'remove' expr 'from' expr            #remove
     | EscapedString                        #atom_string
     | BOOL                                 #atom_bool
     | NUMBER                               #atom_number
     | '{' (expr ',')* expr '}'             #atom_array
     | VARNAME '->' '{' (expr ';')* expr '}'    #function
     | VARNAME                              #variable
     | 'map(' expr ':' expr ')'             #map
     | 'select(' expr ':' expr ')'          #select
     ;


BOOL : 'tt'
     | 'ff'
     ;
VARNAME   : [a-z]+ ;
FUNVAR    : [A-Z]+ ;
EscapedString : '"' ( '""' | ~["\r\n] )* '"';
NUMBER : [0-9]+ (','[0-9]+)? ;
WS     : [ \n\t\r]+ -> skip;