package it.giacomobergami.nestedmodel2.model.logical.expressions;

import it.giacomobergami.nestedmodel2.utils.JPair;

public class PairExpression<T> extends JPair<Expression<T>, Expression<T>> implements Expression<T> {
    public PairExpression(Expression<T> key, Expression<T> value) {
        super(key, value);
    }

    @Override
    public t type() {
        return t.Pair;
    }

    @Override
    public String getString() {
        return toString();
    }

    @Override
    public Integer getInteger() {
        return hashCode();
    }

    @Override
    public JPair<Expression<T>, Expression<T>> getPair() {
        return this;
    }

    @Override
    public T getT() {
        return null;
    }


}
