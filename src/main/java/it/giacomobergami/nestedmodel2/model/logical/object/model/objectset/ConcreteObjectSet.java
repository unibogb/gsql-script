package it.giacomobergami.nestedmodel2.model.logical.object.model.objectset;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.IteratorMap;

import java.util.*;

public class ConcreteObjectSet  implements ObjectSet {
    HashMap<JId, AbstractObject> real = new HashMap<>();

    @Override
    public AbstractObject put(JId key, AbstractObject value) {
        return real.put(key, value);
    }

    @Override
    public AbstractObject get(JId key) {
        return real.get(key);
    }

    @Override
    public Optional<JId> minKey() {
        return real.isEmpty() ? Optional.empty() : Optional.of(Collections.min(real.keySet()));
    }

    @Override
    public Optional<JId> maxKey() {
        return real.isEmpty() ? Optional.empty() : Optional.of(Collections.max(real.keySet()));
    }

    @Override
    public boolean contains(JId key) {
        return real.containsKey(key);
    }

    @Override
    public void setOwner(GSM labelExprGSM) {}

    @Override
    public Iterator<JPair<JId, AbstractObject>> iterator() {
        return new IteratorMap<Map.Entry<JId, AbstractObject>, JPair<JId, AbstractObject>>(real.entrySet().iterator()) {
            @Override
            public JPair<JId, AbstractObject> apply(Map.Entry<JId, AbstractObject> jIdAbstractObjectEntry) {
                return new JPair<>(jIdAbstractObjectEntry.getKey(), jIdAbstractObjectEntry.getValue());
            }
        };
    }

    public void putAll(ConcreteObjectSet x) {
        putAll(x.real);
    }

    public void putAll(Map<JId, AbstractObject> x) {
        real.putAll(x);
    }

    @Override
    public boolean isEmpty() {
        return real.isEmpty();
    }
}
