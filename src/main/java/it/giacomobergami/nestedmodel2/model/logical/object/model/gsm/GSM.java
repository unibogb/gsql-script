package it.giacomobergami.nestedmodel2.model.logical.object.model.gsm;

import it.giacomobergami.nestedmodel2.model.logical.object.model.object.ObjectModel;
import it.giacomobergami.nestedmodel2.model.logical.object.model.objectset.MapObjectSet;
import it.giacomobergami.nestedmodel2.model.logical.object.model.objectset.ObjectSet;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapContainment;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapExpressions;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapLabel;
import it.giacomobergami.nestedmodel2.model.languages.GSQL.DisjointUnion;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.operators.JId.NextJId;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class GSM {
    public final ObjectSet O;
    public JId o;
    public final int currentStep;

    public GSM(int step, ObjectSet internals, JId root) {
        this.o = root;
        this.O = internals;
        O.setOwner(this);
        this.currentStep = step;
    }

    public GSM map(MapLabel mEll, MapExpressions mXi, MapContainment mPhi) {
        return new GSM(currentStep+1,
                new MapObjectSet(O, mEll, mXi, mPhi, null),
                NextJId.instance.apply(o));
    }

    public GSM disjointUnionWith(GSM right) {
        return DisjointUnion.perform(this, right);
    }

    public void create(JId newId, Collection<String> labels, Collection<String> expressions, HashMap<String, ArrayList<JId>> containments) {
        if (!O.contains(newId)) {
            O.put(newId, new ObjectModel(this, newId, labels, expressions, containments));
        } else
            System.err.println("ERROR: " + newId.toString() + " already exists in GSM. I'm not gong to add it");
    }
}
