package it.giacomobergami.nestedmodel2.model.logical.object.model.object;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.IteratorMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ObjectModel extends AbstractObject {
    public final JId _id;
    public final ArrayList<String> _ell;
    public final ArrayList<String> _xi;
    public final HashMap<String, ArrayList<JId>> _phi;
    private final GSM container;

    public ObjectModel(GSM container, JId id) {
        this._id = id;
        this._ell = new ArrayList<>();
        this._xi = new ArrayList<>();
        this._phi = new HashMap<>();
        this.container = container;
    }

    public ObjectModel(GSM container, ObjectModel id) {
        this(container, id._id);
        this._ell.addAll(id._ell);
        this._xi.addAll(id._xi);
        this._phi.putAll(id._phi);
    }

    public ObjectModel(GSM container, JId id, Iterable<String> ell, Iterable<String> _xi, Iterable<JPair<String, Iterator<JId>>> _phi) {
        this(container, id);
        ell.forEach(this._ell::add);
        _xi.forEach(this._xi::add);
        _phi.forEach(x -> {
            ArrayList<JId> l = new ArrayList<>();
            x.value.forEachRemaining(l::add);
            this._phi.put(x.key, l);
        });
    }

    public ObjectModel(GSM container, JId id, Iterable<String> ell, Iterable<String> _xi, HashMap<String, ArrayList<JId>> _phi) {
        this(container, id);
        ell.forEach(this._ell::add);
        _xi.forEach(this._xi::add);
        this._phi.putAll(_phi);
    }

    @Override
    public GSM getContainer() {
        return container;
    }

    @Override
    public JId id() {
        return _id;
    }

    @Override
    public Iterable<String> ell() {
        return _ell;
    }

    @Override
    public Iterable<String> xi() {
        return _xi;
    }

    @Override
    public Iterable<JPair<String, Iterator<JId>>> varphi() {
        return this::getPhiIterator;
    }

    @Override
    public Iterator<String> getEllIterator() {
        return _ell.iterator();
    }

    @Override
    public Iterator<String> getXiIterator() {
        return _xi.iterator();
    }

    @Override
    public Iterator<JPair<String, Iterator<JId>>> getPhiIterator() {
        return new IteratorMap<Map.Entry<String, ArrayList<JId>>,JPair<String, Iterator<JId>>>(_phi.entrySet().iterator()) {
            @Override
            public JPair<String, Iterator<JId>> apply(Map.Entry<String, ArrayList<JId>> o) {
                return new JPair<>(o.getKey(), o.getValue().iterator());
            }
        };
    }

    @Override
    public PhiMap phi() {
        return x -> _phi.get(x);
    }

    @Override
    public AbstractObject copy() {
        return new ObjectModel(container,this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ObjectModel)) return false;

        ObjectModel that = (ObjectModel) o;

        if (_id != null ? !_id.equals(that._id) : that._id != null) return false;
        if (!_ell.equals(that._ell)) return false;
        if (!_xi.equals(that._xi)) return false;
        return _phi != null ? _phi.equals(that._phi) : that._phi == null;
    }

    @Override
    public int hashCode() {
        int result = _id != null ? _id.hashCode() : 0;
        result = 31 * result + _ell.hashCode();
        result = 31 * result + _xi.hashCode();
        result = 31 * result + (_phi != null ? _phi.hashCode() : 0);
        return result;
    }


}
