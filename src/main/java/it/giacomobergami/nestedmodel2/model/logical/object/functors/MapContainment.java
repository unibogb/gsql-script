package it.giacomobergami.nestedmodel2.model.logical.object.functors;

import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;

import java.util.Iterator;
import java.util.function.Function;

public interface MapContainment extends Function<AbstractObject, Iterator<JPair<String, Iterator<JId>>>> {
}
