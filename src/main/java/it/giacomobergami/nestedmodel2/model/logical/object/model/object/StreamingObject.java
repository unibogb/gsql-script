package it.giacomobergami.nestedmodel2.model.logical.object.model.object;

import it.giacomobergami.nestedmodel2.model.logical.expressions.Expression;
import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.IteratorMap;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.Select;

import java.util.ArrayList;
import java.util.Iterator;

public class StreamingObject extends AbstractObject {

    private final JId id;
    private final Iterator ell;
    private final Iterator<String> xi;
    private final Iterator<JPair<String, Iterator<JId>>> phi;
    private final GSM container;

    public StreamingObject(GSM container, JId id, Iterator ell, Iterator<String> xi, Iterator<JPair<String, Iterator<JId>>> phi) {
        this.id = id;
        this.ell = ell;
        this.xi = xi;
        this.phi = phi;
        this.container = container;
    }

    @Override
    public JId id() {
        return id;
    }

    @Override
    public Iterator getEllIterator() {
        return ell;
    }

    @Override
    public Iterator<String> getXiIterator() {
        return xi;
    }

    @Override
    public Iterator<JPair<String, Iterator<JId>>> getPhiIterator() {
        return phi;
    }

    @Override
    public GSM getContainer() {
        return container;
    }
}
