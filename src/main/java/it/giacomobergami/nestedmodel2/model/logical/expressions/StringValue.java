package it.giacomobergami.nestedmodel2.model.logical.expressions;

import it.giacomobergami.nestedmodel2.utils.JPair;

public class StringValue<T> implements Expression<T> {
    private final String string;
    public StringValue(String string) {
        this.string = string;
    }

    @Override
    public t type() {
        return t.String;
    }

    @Override
    public String getString() {
        return string;
    }

    @Override
    public Integer getInteger() {
        return Integer.valueOf(string);
    }

    @Override
    public JPair<Expression<T>, Expression<T>> getPair() {
        return null;
    }

    @Override
    public T getT() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StringValue)) return false;

        StringValue<?> that = (StringValue<?>) o;

        return string != null ? string.equals(that.string) : that.string == null;
    }

    @Override
    public int hashCode() {
        return string != null ? string.hashCode() : 0;
    }
}
