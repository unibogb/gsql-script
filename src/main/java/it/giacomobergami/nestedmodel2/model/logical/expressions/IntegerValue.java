package it.giacomobergami.nestedmodel2.model.logical.expressions;


import it.giacomobergami.nestedmodel2.utils.JPair;

public class IntegerValue<T> implements Expression<T> {
    private final int i;
    public IntegerValue(int i) {
        this.i = i;
    }

    @Override
    public t type() {
        return t.Integer;
    }

    @Override
    public String getString() {
        return ""+i;
    }

    @Override
    public Integer getInteger() {
        return i;
    }

    @Override
    public JPair<Expression<T>, Expression<T>> getPair() {
        return null;
    }
    @Override
    public T getT() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntegerValue)) return false;

        IntegerValue<?> that = (IntegerValue<?>) o;

        return i == that.i;
    }

    @Override
    public int hashCode() {
        return i;
    }
}
