package it.giacomobergami.nestedmodel2.model.logical.object.model.object;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.IteratorMap;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.Select;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class AbstractObject {
    public abstract JId id();
    public abstract Iterator<String> getEllIterator();
    public abstract Iterator<String> getXiIterator();
    public abstract Iterator<JPair<String, Iterator<JId>>> getPhiIterator();
    public PhiMap phi() {
        return x -> {
            ArrayList<JId> toret = new ArrayList<>();
            Iterator<JPair<String, Iterator<JId>>> mi = new Select<JPair<String, Iterator<JId>>>(getPhiIterator()) {
                @Override
                public boolean test(JPair<String, Iterator<JId>> stringIteratorJPair) {
                    return stringIteratorJPair.key.equals(x);
                }
            };
            while (mi.hasNext()) {
                Iterator<JId> current = mi.next().value;
                while (current.hasNext()) {
                    toret.add(current.next());
                }
            }
            return toret;
        };
    }
    public abstract GSM getContainer();

    public interface PhiMap {
        ArrayList<JId> get(String x);
    }

    public Iterable<String> ell() {
        return this::getEllIterator;
    }
    public Iterable<String> xi() {
        return this::getXiIterator;
    }
    public Iterable<JPair<String, Iterator<JId>>> varphi() {
        return this::getPhiIterator;
    }

    /**
     * Joins the objects' id with their internal representation
     * @return
     */
    public Iterable<JPair<String, Iterator<AbstractObject>>> traverse() {
        GSM container = getContainer();
        return () -> new IteratorMap<JPair<String, Iterator<JId>>, JPair<String, Iterator<AbstractObject>>>(getPhiIterator()) {
            @Override
            public JPair<String, Iterator<AbstractObject>> apply(JPair<String, Iterator<JId>> stringIteratorJPair) {
                return new JPair<>(stringIteratorJPair.key, new IteratorMap<JId, AbstractObject>(stringIteratorJPair.value) {
                    @Override
                    public AbstractObject apply(JId jId) {
                        return container.O.get(jId);
                    }
                });
            }
        };
    }

    public AbstractObject copy() {
        return new ObjectModel(getContainer(), id(), ell(), xi(), varphi());
    }

    @Override
    public String toString() {
        JSONObject obj = new JSONObject();
        {
            JSONObject phi = new JSONObject();
            for (JPair<String, Iterator<JId>> x : varphi()) {
                JSONArray e = new JSONArray();
                Iterator<JId> it = x.value;
                while ( it.hasNext()) {
                    e.add(it.next().toString());
                }
                phi.put(x.key, e);
            }
            obj.put("phi", phi);
        }
        {
            JSONArray expressions = new JSONArray();
            xi().forEach(expressions::add);
            obj.put("xi", expressions);
        }
        {
            JSONArray labels = new JSONArray();
            ell().forEach(labels::add);
            obj.put("ell", labels);
        }
        obj.put("id", id().toString());
        return obj.toJSONString();
    }
}
