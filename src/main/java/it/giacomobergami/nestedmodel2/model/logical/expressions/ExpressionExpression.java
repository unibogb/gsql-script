package it.giacomobergami.nestedmodel2.model.logical.expressions;

import it.giacomobergami.nestedmodel2.utils.JPair;

public class ExpressionExpression implements Expression {
    @Override
    public t type() {
        return t.T;
    }
    @Override
    public String getString() {
        return null;
    }
    @Override
    public Integer getInteger() {
        return null;
    }
    @Override
    public JPair<Expression, Expression> getPair() {
        return null;
    }
    @Override
    public Object getT() {
        return this;
    }
}
