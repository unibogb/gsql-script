package it.giacomobergami.nestedmodel2.model.logical.object.model.objectset;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;

import java.util.Optional;

public interface ObjectSet extends Iterable<JPair<JId, AbstractObject>> {
    AbstractObject put(JId key, AbstractObject value);
    AbstractObject get(JId key);
    Optional<JId> minKey();
    Optional<JId> maxKey();
    boolean isEmpty();
    boolean contains(JId key);
    void setOwner(GSM labelExprGSM);
}
