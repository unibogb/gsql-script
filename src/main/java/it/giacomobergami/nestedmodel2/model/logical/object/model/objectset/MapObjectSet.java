package it.giacomobergami.nestedmodel2.model.logical.object.model.objectset;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.model.languages.GSQL.objects.ObjectMap;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapContainment;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapExpressions;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapLabel;
import it.giacomobergami.nestedmodel2.utils.CompareOptional;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;
import it.giacomobergami.nestedmodel2.utils.operators.JId.NextJId;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.Append;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.IteratorMap;

import java.util.Iterator;
import java.util.Optional;

public class MapObjectSet implements ObjectSet {

    private final ObjectSet toMap;
    ConcreteObjectSet overflow = new ConcreteObjectSet();
    private final ObjectMap objectMap;

    public MapObjectSet(ObjectSet toMap,
                        MapLabel labels,
                        MapExpressions expressions,
                        MapContainment containment,
                        GSM owner) {
        this.toMap = toMap;
        this.objectMap = new ObjectMap(labels, expressions, containment, owner);
    }

    public MapObjectSet(ObjectSet toMap,
                        ObjectMap objectMap) {
        this.toMap = toMap;
        this.objectMap = objectMap;
    }

    @Override
    public AbstractObject put(JId key, AbstractObject value) {
        JId next = NextJId.instance.apply(key);
        if (toMap.contains(next)) {
            return objectMap.apply(toMap.put(next, value));
        } else
            return overflow.put(key, value);
    }

    @Override
    public AbstractObject get(JId key) {
        JId next = NextJId.instance.apply(key);
        AbstractObject toret = objectMap.apply(toMap.get(next));
        if (toret == null)
            return overflow.get(key);
        else return toret;
    }

    @Override
    public Optional<JId> minKey() {
        return CompareOptional.min(toMap.minKey(), overflow.minKey());
    }

    @Override
    public Optional<JId> maxKey() {
        return CompareOptional.max(toMap.maxKey(), overflow.maxKey());
    }

    @Override
    public boolean isEmpty() {
        return toMap.isEmpty() && overflow.isEmpty();
    }

    @Override
    public boolean contains(JId key) {
        return toMap.contains(NextJId.instance.apply(key)) || overflow.contains(key);
    }

    @Override
    public void setOwner(GSM labelExprGSM) {
        objectMap.setOwner(labelExprGSM);
    }

    @Override
    public Iterator<JPair<JId, AbstractObject>> iterator() {
        Iterator<JPair<JId, AbstractObject>> it = new IteratorMap<JPair<JId, AbstractObject>, JPair<JId, AbstractObject>>(toMap.iterator()) {
            @Override
            public JPair<JId, AbstractObject> apply(JPair<JId, AbstractObject> abstractObject) {
                return new JPair<>(abstractObject.key,objectMap.apply(abstractObject.value));
            }
        };
        return new Append<JPair<JId, AbstractObject>>(it, overflow.iterator());
    }
}
