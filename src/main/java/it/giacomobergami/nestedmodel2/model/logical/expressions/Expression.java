package it.giacomobergami.nestedmodel2.model.logical.expressions;

import it.giacomobergami.nestedmodel2.utils.JPair;

public interface Expression<T> {
    enum t {
        String,
        Integer,
        Pair,
        T
    }
    t type();
    String getString();
    Integer getInteger();
    JPair<Expression<T>,Expression<T>> getPair();
    T getT();
}
