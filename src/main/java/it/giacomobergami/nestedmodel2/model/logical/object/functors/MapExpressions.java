package it.giacomobergami.nestedmodel2.model.logical.object.functors;

import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;

import java.util.Iterator;
import java.util.function.Function;

public interface MapExpressions extends Function<AbstractObject, Iterator<String>> {
}
