package it.giacomobergami.nestedmodel2.model.languages.GSQL;

import it.giacomobergami.nestedmodel2.model.languages.script.transformations.ScriptASTTransformations;
import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapContainment;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapExpressions;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapLabel;

public class Map {

    public static  GSM map(MapLabel mEll,
                                         MapExpressions mXi,
                                         MapContainment mPhi,
                                         GSM input) {
        return input.map(mEll, mXi, mPhi);
    }

    public static GSM map(String mEll, String mXi, String mPhi, GSM input) {
        MapLabel ell = ScriptASTTransformations.toEll(input, mEll);
        MapExpressions xi = ScriptASTTransformations.toXi(input, mXi);
        MapContainment phi = ScriptASTTransformations.toVarPhi(input, mPhi);
        return Map.map(ell, xi, phi, input);
    }

}
