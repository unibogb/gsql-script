package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.functors;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.Funzione;
import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FilterExpr extends ValueBiFunction {
    public static FilterExpr instance = new FilterExpr(); FilterExpr() {}

    @Override
    public ScriptAST apply(ScriptAST s, ScriptAST f) {
        switch (s.type) {
            case Bool:
            case Integer:
            case Double:
            case String:
                return f.apply(s).toBoolean() ? s : new ScriptAST(s.optGamma);
            case Assignment:
                return s.assign().filter(f);
            case Function: {
                String newPar = s.optGamma.keySet().stream().collect(Collectors.joining()) + s.function.parameter + (f.function != null ? f.function.parameter : "_");
                Funzione concat = new Funzione(s.optGamma, newPar);
                s.function.body.forEach(concat::addExpression);
                if (!concat.body.isEmpty()) {
                    int pos = concat.body.size() - 1;
                    ScriptAST old = concat.body.get(pos);
                    concat.body.set(pos, f.apply(old).toBoolean() ? old : new ScriptAST(s.optGamma));
                }
                return new ScriptAST(s.optGamma, concat);
            }
            case Array: {
                ArrayList<ScriptAST> map = new ArrayList<>();
                for (ScriptAST x : s.arrayList) {
                    if (f.apply(x).toBoolean())
                        map.add(x);
                }
                return new ScriptAST(s.optGamma, map);
            }
            case Java:
            case JavaMethod:
            case Variable:
            case LazyExpression:
                return s.evalPriorToApplication(x -> x.filter(f));
        }
        return f.apply(s);
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "select(" + x.get(0).stringSerialize()+" : "+x.get(1).stringSerialize() +")";
    }
}
