package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.expressions;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;

import java.util.ArrayList;
import java.util.List;

public class Remove extends ValueBiFunction {
    public final static Remove instance = new Remove(); Remove() {}

    @Override
    public ScriptAST apply(ScriptAST dis, ScriptAST position) {
        ArrayList<ScriptAST> x = dis.toList();
        int pos = (int) position.toInteger().toLong();
        if (x.size() > pos && pos >= 0) {
            ScriptAST toret = x.remove(pos);
            if (dis.optGamma != null) {
                dis.optGamma.put(dis.string != null ? dis.string : dis.toString(), dis);
            }
            return toret;
        } else return new ScriptAST(dis.optGamma);
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "remove " + x.get(0).stringSerialize()+" from "+x.get(1).stringSerialize() +"";
    }
}
