package it.giacomobergami.nestedmodel2.model.languages.script.structures;

import it.giacomobergami.nestedmodel2.model.languages.script.annotations.Expression;
import it.giacomobergami.nestedmodel2.model.languages.script.annotations.MetaOperation;
import it.giacomobergami.nestedmodel2.model.languages.script.annotations.Statement;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.functors.SubElements;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueFunction;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueTerFunction;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.bools.*;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.expressions.*;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.functors.*;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.numbers.Add;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.numbers.Div;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.numbers.Minus;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.numbers.Mul;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.serializable.SerializableListFunctions;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.string.Concat;
import it.giacomobergami.nestedmodel2.model.languages.script.transformations.ScriptASTTransformations;
import it.giacomobergami.nestedmodel2.utils.IdFunction;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;

import java.math.BigInteger;
import java.util.*;
import java.util.function.*;

public class ScriptAST implements Comparable<ScriptAST> {
    public Boolean bool; // 0
    public JId integer; // 1
    public Double doubleValue;
    public String string; // 2
    public ArrayList<ScriptAST> arrayList; // 3
    public JPair<ScriptAST, ScriptAST> assignment;
    public JPair<List<ScriptAST>, SerializableListFunctions> argsWithFunction;
    public Funzione function;
    public Javification object;
    public t type;
    public Gamma optGamma;

    /*
     * Runs the expressions, while using lazy evaluation for functions and variables
     */
    @MetaOperation
    public ScriptAST run() {
        switch (type) {
            case Bool:
                toBoolean();
                return this;
            case Integer:
                toInteger();
                return this;
            case Double:
                toDouble();
                return this;
            case String:
                toString();
                return this;
            case Array:
                toList();
                return this;
            case Function:
                return this;
            case Java:
            case JavaMethod:
            case LazyExpression:
            case Assignment:
                return evalPriorToApplication(ScriptAST::run);
                //return assignmentAssign();
            case Variable:
                return variableEval();
        }
        return new ScriptAST(null);
    }

    @MetaOperation
    public ScriptAST assign() {
        switch (type) {
            case Assignment:
                return assignmentAssign();
            default:
                return this;
        }
    }

    /**
     * Run simply returns the variable, while this evaluates everythin as an expression. Forces the evaluation of lazy
     * functions
     * <p>
     * expr1 !
     *
     * @return
     */
    @MetaOperation
    @Expression
    public ScriptAST eval() {
        switch (type) {
            case Variable:
                if (optGamma == null) return new ScriptAST(null);
                else return optGamma.get(string);
            default:
                return run();
        }
    }

    /**
     * (expr1 expr2)
     */
    @MetaOperation
    @Expression
    public ScriptAST apply(ScriptAST toApply) {
        return createBiFunction(Apply.instance,this,toApply);
    }

    /**
     * var(expr1)
     */
    @Expression
    public ScriptAST var() {
        switch (type) {
            case Variable:
                return this;
            default:
                return ScriptAST.variable(optGamma, toString());
        }
    }

    @Expression
    public ScriptAST and(ScriptAST x) { return createBiFunction(And.instance, this, x); }

    @Expression
    public ScriptAST or(ScriptAST x) { return createBiFunction(Or.instance, this, x); }

    @Expression
    public ScriptAST not() { return createFunction(Not.instance, this); }

    @Expression
    public ScriptAST imply(ScriptAST x) { return createBiFunction(Imply.instance, this, x); }

    @Expression
    public ScriptAST ifte(ScriptAST l, ScriptAST r) { return createTerFunction(Ifte.instance, this, l, r); }

    @Expression
    public ScriptAST add(ScriptAST right) { return createBiFunction(Add.instance, this, right); }

    @Expression
    public ScriptAST diff(ScriptAST right) { return createBiFunction(Minus.instance, this, right); }

    @Expression
    public ScriptAST div(ScriptAST x) { return createBiFunction(Div.instance, this, x); }

    @Expression
    public ScriptAST mult(ScriptAST x) { return createBiFunction(Mul.instance, this, x); }

    @Expression
    public ScriptAST concat(ScriptAST x) { return createBiFunction(Concat.instance, this, x); }

    @Expression
    public ScriptAST substring(ScriptAST left, ScriptAST right) { return createTerFunction(SubElements.instance, this, left, right); }

    @Expression
    public ScriptAST append(ScriptAST x) { return createBiFunction(Append.instance, this, x); }

    @Expression
    public ScriptAST at(ScriptAST x) { return createBiFunction(At.instance, this, x); }

    @Statement
    public ScriptAST put(ScriptAST arg1, ScriptAST arg2) { return createTerFunction(new Put(), this, arg1, arg2); }

    @Expression
    public ScriptAST contains(ScriptAST right) { return createBiFunction(Contains.instance, this, right); }

    @Expression
    public ScriptAST remove(ScriptAST param) { return createBiFunction(Remove.instance, this, param); }

    @Expression
    public ScriptAST eq(ScriptAST x) { return createBiFunction(Eq.instance, this, x); }

    @Expression
    public ScriptAST neq(ScriptAST x) {
        return eq(x).not();
    }

    @Expression
    public ScriptAST lt(ScriptAST x) { return createBiFunction(Less.instance, this, x); }

    @Expression
    public ScriptAST gt(ScriptAST x) { return createBiFunction(More.instance, this, x); }

    @Expression
    public ScriptAST leq(ScriptAST x) {
        return gt(x).not();
    }

    @Expression
    public ScriptAST geq(ScriptAST x) {
        return lt(x).not();
    }

    /**
     * this := y
     */
    @Statement
    public ScriptAST assign(ScriptAST y) { return createBiFunction(Assign.instance, false,this, y); }

    /* this.y */
    @Statement
    public ScriptAST invoke(ScriptAST y) { return createBiFunction(Invoke.instance, this, y); }

    @Statement
    public ScriptAST map(ScriptAST function) { return createBiFunction(MapFunctor.instance, this, function); }

    @Statement
    public ScriptAST filter(ScriptAST prop) { return createBiFunction(FilterExpr.instance, this, prop); }

    public Iterable<String> toStringIterable() {
        return ScriptASTTransformations.toStringIterable(this);
    }

    public Iterable<JPair<String, Iterator<JId>>> toPhiId() {
        return ScriptASTTransformations.toPhiId(this);
    }

    /*private Values evalObject() {
        return object.toValue();
    }

    private Values evalMethod() {
        return object.invoke(string);
    }

    private Values forceLazyEvaluation() {
        return argsWithFunction.value.apply(argsWithFunction.key);
    }*/

    public enum t {
        Bool,
        Integer,
        Double,
        String,
        Assignment,
        Function,
        Array,
        Java,
        JavaMethod,
        Variable,
        LazyExpression
    }

    /////////////////////// CONVERTORS

    public boolean toBoolean() {
        switch (type) {
            case Bool:
                return bool;
            case Integer:
                return integer.equals(JId.ONE);
            case Double:
                return doubleValue != 0;
            case String:
                return !string.isEmpty();
            case Function:
                return !function.body.isEmpty();
            case Array:
                return !arrayList.isEmpty();
            case Assignment:
            case Java:
            case JavaMethod:
            case Variable:
            case LazyExpression:
                return evalPriorToApplication(ScriptAST::toBoolean);
            default:
                return false;
        }
    }

    public ArrayList<ScriptAST> toList() {
        switch (type) {
            case Array:
                return arrayList;
            case Function:
                return function.body;
            case LazyExpression:
            case Variable:
            case Java:
            case JavaMethod:
                return evalPriorToApplication(ScriptAST::toList);
            default: {
                ArrayList<ScriptAST> toreturn = new ArrayList<>();
                toreturn.add(type.equals(t.Assignment) ? assign() : this);
                return toreturn;
            }
        }
    }

    public Object toJavaObject() {
        switch (type) {
            case Bool:
                return bool;
            case Integer:
                return integer;
            case Double:
                return doubleValue;
            case String:
                return string;
            case Function:
                return function.body;
            case Array:
                return arrayList;
            case Java:
                return object.reference;
            case JavaMethod:
                return object.invoke(ScriptAST.string(optGamma, string));
            case Variable:
            case LazyExpression:
            case Assignment:
                return evalPriorToApplication();
        }
        return new ArrayList<>();
    }

    public JId toInteger() {
        switch (type) {
            case Bool:
                return bool ? JId.ONE : JId.ZERO;
            case Integer:
                return integer;
            case Double:
                Double d = doubleValue;
                return new JId(d.intValue());
            case String: {
                try {
                    return new JId(new BigInteger(string));
                } catch (Exception e) {
                    return JId.ZERO;
                }
            }
            case Function:
                return new JId(function.body.size());
            case Array:
                return new JId(arrayList.size());
            case Assignment:
            case Java:
            case JavaMethod:
            case Variable:
            case LazyExpression:
                return evalPriorToApplication(ScriptAST::toInteger);
            default:
                return JId.ZERO;
        }
    }

    public double toDouble() {
        switch (type) {
            case Bool:
                return bool ? 1 : 0;
            case Integer:
                return (double) integer.toLong();
            case Double:
                return doubleValue;
            case String: {
                try {
                    return Double.valueOf(string);
                } catch (Exception e) {
                    return 0;
                }
            }
            case Function:
                return function.body.size();
            case Array:
                return arrayList.size();
            case Assignment:
            case Java:
            case JavaMethod:
            case Variable:
            case LazyExpression:
                return evalPriorToApplication(ScriptAST::toDouble);
            default:
                return 0;
        }
    }

    @Override
    public String toString() {
        switch (type) {
            case Bool:
                return Boolean.toString(bool);
            case Integer:
                return integer.toString();
            case Double:
                return doubleValue + "";
            case String:
                return string;
            case Function:
                return function.toString();
            case Array: {
                StringBuilder sb = new StringBuilder();
                sb.append('{');
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    Object curr = it.next();
                    sb.append(curr.toString());
                    if (it.hasNext())
                        sb.append(", ");
                }
                sb.append('}');
                return sb.toString();
            }
            case Assignment:
                return assignment.key + " := " + assignment.value;
            case Java:
            case JavaMethod:
            case LazyExpression:
                return evalPriorToApplication(ScriptAST::toString);
            case Variable: {
                ScriptAST e = eval();
                return e == null ? string : e.toString();
            }
            default:
                return "";
        }
    }

    public String stringSerialize() {
        switch (type) {
            case Bool:
                return bool ? "tt" : "ff";
            case Integer:
                return integer.toString();
            case Double:
                return (doubleValue + "").replace('.',',');
            case String:
                return  (true ? "\"" : "") + string + (true ? "\"" : "") ;
            case Function:
                return function.toString();
            case Array: {
                StringBuilder sb = new StringBuilder();
                sb.append('{');
                Iterator<ScriptAST> it = arrayList.iterator();
                while (it.hasNext()) {
                    ScriptAST curr = it.next();
                    sb.append(curr.stringSerialize());
                    if (it.hasNext())
                        sb.append(", ");
                }
                sb.append('}');
                return sb.toString();
            }
            case Assignment:
                return assignment.key.stringSerialize() + " := " + assignment.value.stringSerialize();
            case Java:
            case JavaMethod:
                return evalPriorToApplication(x -> x.stringSerialize());
            case LazyExpression:
                return argsWithFunction.value.toString(argsWithFunction.key);
            case Variable: {
                return optGamma.containsKey(string) ? optGamma.get(string).stringSerialize() : string;
            }
            default:
                return "";
        }
    }

    /////////////////// Java Utils

    /**
     * Implements the structural equality.
     * Use the Compare in order to achieve the value equality
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScriptAST)) return false;

        ScriptAST values = (ScriptAST) o;

        if (bool != values.bool) return false;
        if (integer != null ? !integer.equals(values.integer) : values.integer != null) return false;
        if (doubleValue != null ? !doubleValue.equals(values.doubleValue) : values.doubleValue != null) return false;
        if (string != null ? !string.equals(values.string) : values.string != null) return false;
        if (arrayList != null ? !arrayList.equals(values.arrayList) : values.arrayList != null) return false;
        if (assignment != null ? !assignment.equals(values.assignment) : values.assignment != null) return false;
        if (function != null ? !function.equals(values.function) : values.function != null) return false;
        if (type != values.type) return false;
        return optGamma != null ? optGamma.equals(values.optGamma) : values.optGamma == null;
    }

    @Override
    public int hashCode() {
        int result = (bool ? 1 : 0);
        result = 31 * result + (integer != null ? integer.hashCode() : 0);
        result = 31 * result + (doubleValue != null ? doubleValue.hashCode() : 0);
        result = 31 * result + (string != null ? string.hashCode() : 0);
        result = 31 * result + (arrayList != null ? arrayList.hashCode() : 0);
        result = 31 * result + (assignment != null ? assignment.hashCode() : 0);
        result = 31 * result + (function != null ? function.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (optGamma != null ? optGamma.hashCode() : 0);
        return result;
    }

    private static int compareArrayValues(ArrayList<ScriptAST> left, ArrayList<ScriptAST> right) {
        int cmp;
        cmp = Integer.compare(left.size(), right.size());
        if (cmp != 0)
            return cmp;
        else {
            int msize = Integer.min(left.size(), right.size());
            for (int i = 0; i < msize; i++) {
                cmp = left.get(i).compareTo(right.get(i));
                if (cmp != 0)
                    return cmp;
            }
            return 0;
        }
    }

    @Override
    public int compareTo(ScriptAST o) {
        if (type.equals(t.Java)) {
            return object.toValue().compareTo(o);
        } else if (type.equals(t.JavaMethod)) {
            return object.invoke(string).compareTo(o);
        } else if (o.type.equals(t.Java)) {
            return this.compareTo(o.object.toValue());
        } else if (o.type.equals(t.JavaMethod)) {
            return this.compareTo(o.object.invoke(o.string));
        }

        int thiscmp = type.ordinal();
        int ocmp = o.type.ordinal();
        int cmp = Integer.compare(thiscmp, ocmp);
        if (cmp != 0)
            return cmp;
        else {
            switch (o.type) {
                case Assignment:
                    return assign().compareTo(o.assign());
                case Bool:
                    return Boolean.compare(bool, o.bool);
                case Integer:
                    return toInteger().compareTo(o.toInteger());
                case Double:
                    return Double.compare(toDouble(), o.toDouble());
                case String:
                    return string.compareTo(o.string);
                case Function: {
                    cmp = function.parameter.compareTo(o.function.parameter);
                    if (cmp != 0)
                        return cmp;
                    else return compareArrayValues(function.body, o.function.body);
                }
                case Array:
                    return compareArrayValues(arrayList, o.arrayList);
                case Java:
                    System.err.println("Error: unexpected Java");
                    return this.compareTo(o.object.toValue());
                case JavaMethod:
                    System.err.println("Error: unexpected JavaMethod");
                    return this.compareTo(o.object.invoke(o.string));
                case Variable:
                    return eval().compareTo(o.eval());
                default:
                    return -1;
            }
        }
    }

    public String print() {
        return print(true);
    }

    public String print(boolean out) {
        String str = stringSerialize();
        if (out) System.out.println(str);
        return str;
    }

    ///////////////////  Constructors

    public ScriptAST(Gamma g, boolean bool) {
        this(g, t.Bool);
        this.bool = bool;
    }

    public ScriptAST(Gamma g, SerializableListFunctions fun, ScriptAST... args) {
        this(g, t.LazyExpression);
        argsWithFunction = new JPair<>(Arrays.asList(args), fun);
    }

    public ScriptAST(Gamma g, String i) {
        this(g, t.Integer);
        this.integer = new JId(new BigInteger(i));
    }

    public ScriptAST(Gamma g, JId i) {
        this(g, t.Integer);
        this.integer = new JId(i);
    }

    public ScriptAST(Gamma g, int i) {
        this(g, t.Integer);
        this.integer = new JId(i);
    }

    public ScriptAST(Gamma g, double d) {
        this(g, t.Double);
        this.doubleValue = d;
    }

    private ScriptAST(Gamma g, t stringType, String s) {
        this(g, stringType);
        this.string = s;
    }

    public ScriptAST(Gamma g, Object o) {
        this(g, t.Java);
        this.object = new Javification(o);
    }

    public ScriptAST(Gamma g, ScriptAST... elements) {
        this(g, t.Array);
        this.arrayList = new ArrayList<>(Arrays.asList(elements));
    }

    public ScriptAST(Gamma g, ArrayList<ScriptAST> elements) {
        this(g, t.Array);
        this.arrayList = ((elements));
    }

    public ScriptAST(Gamma g, Funzione elements) {
        this(g, t.Function);
        this.function = elements;
    }

    public ScriptAST(Gamma g, t type) {
        this.type = type;
        this.optGamma = g;
    }

    public ScriptAST(Gamma g) {
        this(g, t.Array);
        this.arrayList = new ArrayList<>();
    }

    public ScriptAST(Gamma g, String method, ScriptAST object) {
        this(g, t.JavaMethod);
        this.string = method;
        this.object = new Javification(optGamma, object);
    }

    public static ScriptAST string(Gamma g, String s) {
        return new ScriptAST(g, t.String, s);
    }

    public static ScriptAST variable(Gamma g, String s) {
        return new ScriptAST(g, t.Variable, s);
    }

    public static ScriptAST assignment(Gamma g, ScriptAST left, ScriptAST right) {
        ScriptAST v = new ScriptAST(g, t.Assignment);
        v.assignment = new JPair<>(left, right);
        return v;
    }

    ///////////////////  Other utils

    public boolean isDouble() {
        if (type.equals(t.String)) {
            try {
                Double.valueOf(string);
                return true;
            } catch (Exception e) {
                return false;
            }
        } else
            return type.equals(t.Double);
    }

    public void setContext(Gamma g) {
        switch (type) {
            case Assignment:
                assignment.key.setContext(g);
                assignment.value.setContext(g);
                break;
            case Array:
                optGamma = g;
                for (int i = 0; i < arrayList.size(); i++) {
                    arrayList.get(i).setContext(g);
                }
                break;
            case Function:
                function.setContext(g);
                break;
            case JavaMethod:
            case Java:
                object.gamma = g;
                break;
            case LazyExpression:
                for (int i = 0; i < argsWithFunction.key.size(); i++) {
                    argsWithFunction.key.get(i).setContext(g);
                }
                break;
        }
        optGamma = g;
    }

    private ScriptAST variableEval() {
        if (optGamma == null) {
            System.err.println("Error: variable cannot be solved: Gamma is null");
            return new ScriptAST(null);
        } else
            return optGamma.get(string);
    }

    private ScriptAST assignmentAssign() {
        if (optGamma != null)
            assignment.key.optGamma = optGamma;
        return assignment.key.assign(assignment.value);
    }

    public <T> T evalPriorToApplication(Function<ScriptAST, T> fun) {
        ScriptAST extern = this;
        switch (type) {
            case Java:
                extern =  object.toValue();
                break;
            case JavaMethod:
                extern = object.invoke(string);
                break;
            case LazyExpression:
                extern = argsWithFunction.value.apply(argsWithFunction.key);
                break;
            case Variable:
                extern = variableEval();
                break;
            case Assignment:
                extern = assignmentAssign();
                break;
        }
        return fun.apply(extern);
    }

    private ScriptAST evalPriorToApplication() {
        return this.evalPriorToApplication(IdFunction.valuesInstance);
    }

    private ScriptAST createFunction(ValueFunction bifun, ScriptAST arg0) {
        return new ScriptAST(arg0.optGamma, new ListValuesFunction(bifun, arg0.optGamma), arg0);
    }

    private ScriptAST createBiFunction(ValueBiFunction bifun, ScriptAST arg0, ScriptAST arg1) {
        return createBiFunction(bifun, true, arg0, arg1);
    }

    private ScriptAST createBiFunction(ValueBiFunction bifun, boolean x, ScriptAST arg0, ScriptAST arg1) {
        return new ScriptAST(arg0.optGamma, new ListValuesBiFunction(bifun, arg0.optGamma, x), arg0, arg1);
    }

    private ScriptAST createTerFunction(ValueTerFunction bifun, ScriptAST arg0, ScriptAST arg1, ScriptAST arg2) {
        return new ScriptAST(arg0.optGamma, new ListValuesTerFunction(bifun, arg0.optGamma), arg0, arg1, arg2);
    }

    private static class ListValuesTerFunction extends SerializableListFunctions {
        private final ValueTerFunction bifun;
        private final Gamma arg0;
        public ListValuesTerFunction(ValueTerFunction bifun, Gamma arg0) {
            this.bifun = bifun;
            this.arg0 = arg0;
        }
        @Override
        public ScriptAST apply(List<ScriptAST> ls) {
            return ls != null ? (bifun.apply(ls.get(0).<ScriptAST>evalPriorToApplication(), ls.get(1), ls.get(2))) : new ScriptAST(arg0);
        }

        @Override
        public String toString(List<ScriptAST> x) {
            return bifun.toString(x);
        }
    }

    private static class ListValuesBiFunction extends SerializableListFunctions {
        private final ValueBiFunction bifun;
        private final Gamma arg0;
        private final boolean leftReduce;

        public ListValuesBiFunction(ValueBiFunction bifun, Gamma arg0, boolean leftReduce) {
            this.bifun = bifun;
            this.arg0 = arg0;
            this.leftReduce = leftReduce;
        }

        @Override
        public ScriptAST apply(List<ScriptAST> ls) {
            ScriptAST curr = ls.get(0);
            curr = leftReduce ? curr.<ScriptAST>evalPriorToApplication() : curr;
            return ls != null ? (bifun.apply(curr, ls.get(1))) : new ScriptAST(arg0);
        }

        @Override
        public String toString(List<ScriptAST> x) {
            return bifun.toString(x);
        }
    }

    private static class ListValuesFunction extends SerializableListFunctions {
        private final ValueFunction bifun;
        private final Gamma arg0;

        public ListValuesFunction(ValueFunction bifun, Gamma arg0) {
            this.bifun = bifun;
            this.arg0 = arg0;
        }

        @Override
        public ScriptAST apply(List<ScriptAST> ls) {
            return ls != null ? (bifun.apply(ls.get(0).<ScriptAST>evalPriorToApplication())) : new ScriptAST(arg0);
        }

        @Override
        public String toString(List<ScriptAST> x) {
            return bifun.toString(x);
        }
    }
}
