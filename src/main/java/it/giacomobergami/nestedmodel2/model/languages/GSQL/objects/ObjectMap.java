package it.giacomobergami.nestedmodel2.model.languages.GSQL.objects;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.StreamingObject;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapContainment;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapExpressions;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapLabel;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;
import it.giacomobergami.nestedmodel2.utils.operators.JId.NextJId;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.IteratorMap;

import java.util.Iterator;
import java.util.function.Function;

public class ObjectMap implements Function<AbstractObject, AbstractObject> {

    private final MapLabel mEll;
    private final MapExpressions mXi;
    private final MapContainment mPhi;
    private GSM input;

    public ObjectMap(MapLabel mEll, MapExpressions mXi, MapContainment mPhi, GSM input) {
        this.mEll = mEll;
        this.mXi = mXi;
        this.mPhi = mPhi;
        this.input = input;
    }

    @Override
    public AbstractObject apply(AbstractObject leAbstractObject) {
        if (leAbstractObject == null) return null;
        Iterator<JPair<String, Iterator<JId>>> mmphi =
                new IteratorMap<JPair<String, Iterator<JId>>, JPair<String, Iterator<JId>>>(mPhi.apply(leAbstractObject)) {
                    @Override
                    public JPair<String, Iterator<JId>> apply(JPair<String, Iterator<JId>> expressionIteratorJPair) {
                        expressionIteratorJPair.value = new IteratorMap<JId, JId>(expressionIteratorJPair.value) {
                            @Override
                            public JId apply(JId jId) {
                                return NextJId.instance.apply(jId);
                            }
                        };
                        return expressionIteratorJPair;
                    }
                };
        return new StreamingObject(input, leAbstractObject.id(), mEll.apply(leAbstractObject), mXi.apply(leAbstractObject), mmphi);
    }

    public void setOwner(GSM owner) {
        this.input = owner;
    }
}
