package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.string;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueTerFunction;

public abstract class StringTerFunction extends ValueTerFunction {
    protected abstract String map(String left, String middle, String right);
    @Override
    public ScriptAST apply(ScriptAST left, ScriptAST middle, ScriptAST right) {
        return ScriptAST.string(left.optGamma,map(left.toString(), middle.toString(), right.toString()));
    }
}
