package it.giacomobergami.nestedmodel2.model.languages.GSQL;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.model.languages.GSQL.objects.DisjointUnionObject;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.IteratorMap;
import it.giacomobergami.nestedmodel2.utils.operators.objectSet.Append;
import it.giacomobergami.nestedmodel2.model.logical.object.model.objectset.ObjectSet;

import java.util.stream.StreamSupport;

public class DisjointUnion {

    public static  GSM perform(GSM left, GSM right) {
        ObjectSet os = Append.append(left.O, right.O);
        Iterable<JId> rightIterator = () -> new IteratorMap<JPair<JId, AbstractObject>, JId>(os.iterator()) {
            @Override
            public JId apply(JPair<JId, AbstractObject> jIdAbstractObjectJPair) {
                return jIdAbstractObjectJPair.key.undovetail().value;
            }
        };
        int maxStep = Integer.max(left.currentStep, right.currentStep);
        JId max = StreamSupport.stream(rightIterator.spliterator(), false).max(JId::compareTo).orElse(JId.ZERO);
        JId gc = JId.dovetail(new JId(maxStep), max.add(1));
        GSM toret = new GSM(maxStep, os, gc);
        DisjointUnionObject newRoot = new DisjointUnionObject(toret, gc, left.O.get(left.o), right.O.get(right.o));
        toret.O.put(gc, newRoot);
        return toret;
    }

}
