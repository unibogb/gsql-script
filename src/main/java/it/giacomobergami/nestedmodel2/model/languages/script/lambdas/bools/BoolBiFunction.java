package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.bools;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;

public abstract class BoolBiFunction extends ValueBiFunction {
    protected abstract boolean rawApply(ScriptAST left, ScriptAST right);
    @Override
    public ScriptAST apply(ScriptAST left, ScriptAST right) {
        return new ScriptAST(left.optGamma, rawApply(left, right));
    }
}