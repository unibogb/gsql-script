package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.bools;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;

import java.util.List;

public class Or extends BoolBiFunction {
    public static final Or instance = new Or();
    private Or() { }
    @Override
    protected boolean rawApply(ScriptAST left, ScriptAST right) {
        return left.toBoolean() || right.toBoolean();
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "(" + x.get(0).stringSerialize()+") || ("+x.get(1).stringSerialize() +")";
    }
}
