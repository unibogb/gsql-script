package it.giacomobergami.nestedmodel2.model.languages.GSQL;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.ObjectModel;
import it.giacomobergami.nestedmodel2.utils.JId;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class Create {

    public static GSM perform(GSM operand, Collection<String> ells, Collection<String> expressions, HashMap<String, ArrayList<JId>> phi){
        operand.create(new JId(1),ells,expressions,phi);
        return operand;
    }

}
