package it.giacomobergami.nestedmodel2.model.languages.script.transformations;

import it.giacomobergami.nestedmodel2.model.languages.script.ScriptLanguage;
import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapContainment;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapExpressions;
import it.giacomobergami.nestedmodel2.model.logical.object.functors.MapLabel;
import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;

import java.util.ArrayList;
import java.util.Iterator;

public class ScriptASTTransformations {

    public static Iterable<String> toStringIterable(ScriptAST x) {
        ArrayList<String> toret = new ArrayList<>(x.toList().size());
        for (ScriptAST y : x.toList()) {
            toret.add(y.stringSerialize());
        }
        return toret;
    }

    public static Iterable<JPair<String, Iterator<JId>>> toPhiId(ScriptAST x) {
        ArrayList<JPair<String, Iterator<JId>>> toret = new ArrayList<>(x.toList().size());
        for (ScriptAST y : x.toList()) {
            ArrayList<ScriptAST> yls = y.toList();
            String s = yls.get(0).stringSerialize();
            ArrayList<ScriptAST> ylsRight = yls.get(1).toList();
            ArrayList<JId> toIt = new ArrayList<>(ylsRight.size());
            for (ScriptAST z : ylsRight) {
                toIt.add(z.toInteger());
            }
            toret.add(new JPair<>(s, toIt.iterator()));
        }
        return toret;
    }

    public static MapLabel toEll(final GSM x, final String script) {
        return abstractObject -> new ScriptLanguage(script)
          .setObjectO(abstractObject)
          .setFunctionF(abstractObject.ell())
          .setGSM(x)
          .evaluate()
          .toStringIterable()
          .iterator();
    }

    public static MapExpressions toXi(final GSM x, final String script) {
        return abstractObject -> new ScriptLanguage(script)
                .setObjectO(abstractObject)
                .setFunctionF(abstractObject.xi())
                .setGSM(x)
                .evaluate()
                .toStringIterable()
                .iterator();
    }

    public static MapContainment toVarPhi(final GSM x, final String script) {
        return abstractObject -> new ScriptLanguage(script)
                .setObjectO(abstractObject)
                .setFunctionF(abstractObject.varphi())
                .setGSM(x)
                .evaluate()
                .toPhiId()
                .iterator();
    }

}
