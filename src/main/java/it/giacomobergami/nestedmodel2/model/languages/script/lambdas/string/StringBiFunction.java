package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.string;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;

public abstract class StringBiFunction extends ValueBiFunction{
    protected abstract String map(String left, String right);
    @Override
    public ScriptAST apply(ScriptAST left, ScriptAST right) {
        return ScriptAST.string(left.optGamma,map(left.toString(), right.toString()));
    }
}
