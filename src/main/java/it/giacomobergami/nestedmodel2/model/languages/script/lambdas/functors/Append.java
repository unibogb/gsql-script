package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.functors;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;

import java.util.ArrayList;
import java.util.List;

public class Append extends ValueBiFunction {
    public final static ValueBiFunction instance = new Append(); Append() {}
    @Override
    public ScriptAST apply(ScriptAST z, ScriptAST y) {
        ArrayList<ScriptAST> toret = new ArrayList<>();
        toret.addAll(z.toList());
        toret.addAll(y.toList());
        return new ScriptAST(z.optGamma, toret);
    }

    @Override
    public String toString(List<ScriptAST> x) {
        return "(" + x.get(0).stringSerialize()+") @ ("+x.get(1).stringSerialize() +")";
    }
}
