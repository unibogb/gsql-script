package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.serializable;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;

import java.util.List;

public interface SerializableFunction {
    String toString(List<ScriptAST> x);
}
