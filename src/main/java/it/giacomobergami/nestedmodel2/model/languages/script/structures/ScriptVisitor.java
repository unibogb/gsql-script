package it.giacomobergami.nestedmodel2.model.languages.script.structures;


import it.giacomobergami.nestedmodel2.model.languages.script.structures.Funzione;
import it.giacomobergami.nestedmodel2.model.languages.script.structures.Gamma;
import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.model.logical.scriptlanguage.nslParser;
import it.giacomobergami.nestedmodel2.model.logical.scriptlanguage.nslVisitor;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

import java.util.ArrayList;
import java.util.Map;

public class ScriptVisitor extends AbstractParseTreeVisitor<ScriptAST> implements nslVisitor<ScriptAST> {

    private Gamma context;
    public ScriptVisitor() {
        context = new Gamma();
    }

    @Override
    public ScriptAST visitNsl(nslParser.NslContext ctx) {
        ScriptAST result = new ScriptAST(context);
        for (nslParser.ExprContext x : ctx.expr()) {
            result = visit(x)
                    .run();
        }
        return result;
    }

    @Override
    public ScriptAST visitParen(nslParser.ParenContext ctx) {
        return visit(ctx.expr());
    }

    /*
     *          Meta Operations
     */
    @Override
    public ScriptAST visitEval(nslParser.EvalContext ctx) {
        return visit(ctx.expr()).eval();
    }

    @Override
    public ScriptAST visitVar(nslParser.VarContext ctx) {
        return visit(ctx.expr()).var();
    }

    /*
    *    Atomic Values
    */
    @Override
    public ScriptAST visitAtom_string(nslParser.Atom_stringContext ctx) {
        String x = ctx.EscapedString().getSymbol().getText();
        x = x.substring(1, x.length()-1);
        return ScriptAST.string(context, x);
    }

    @Override
    public ScriptAST visitAtom_number(nslParser.Atom_numberContext ctx) {
        String x = ctx.NUMBER().getSymbol().getText();
        String y = x.replaceFirst(",",".");
        if (x.equals(y)) {
            return new ScriptAST(context, x);
        } else {
            return new ScriptAST(context, Double.valueOf(y));
        }
    }

    @Override
    public ScriptAST visitAtom_array(nslParser.Atom_arrayContext ctx) {
        ArrayList<ScriptAST> al = new ArrayList<>();
        for (nslParser.ExprContext e : ctx.expr()) {
            al.add(visit(e));
        }
        return new ScriptAST(context, al);
    }

    @Override
    public ScriptAST visitSelect(nslParser.SelectContext ctx) {
        return visit(ctx.expr(0)).filter(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitAtom_bool(nslParser.Atom_boolContext ctx) {
        String s = ctx.BOOL().getSymbol().getText();
        if (s.equals("tt"))
            return new ScriptAST(context, true);
        if (s.equals("ff"))
            return new ScriptAST(context, false);
        System.err.println("ERROR on boolean = " + s);
        return new ScriptAST(context, false);
    }

    @Override
    public ScriptAST visitVariable(nslParser.VariableContext ctx) {
        return ScriptAST.variable(context, ctx.getText());
    }

    /*
     *          Integer Operations
     */

    @Override
    public ScriptAST visitSub(nslParser.SubContext ctx) {
        return visit(ctx.expr(0)).diff(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitMult(nslParser.MultContext ctx) {
        return visit(ctx.expr(0)).mult(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitAdd(nslParser.AddContext ctx) {
        return visit(ctx.expr(0)).add(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitDiv(nslParser.DivContext ctx) {
        return visit(ctx.expr(0)).div(visit(ctx.expr(1)));
    }

    /*
     *          Comparators
     */
    @Override
    public ScriptAST visitLt(nslParser.LtContext ctx) {
        return visit(ctx.expr(0)).lt(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitGeq(nslParser.GeqContext ctx) {
        return visit(ctx.expr(0)).geq(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitLeq(nslParser.LeqContext ctx) {
        return visit(ctx.expr(0)).leq(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitNeq(nslParser.NeqContext ctx) {
        return visit(ctx.expr(0)).neq(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitMap(nslParser.MapContext ctx) {
        return visit(ctx.expr(0)).map(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitEq(nslParser.EqContext ctx) {
        return visit(ctx.expr(0)).eq(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitGt(nslParser.GtContext ctx) {
        return visit(ctx.expr(0)).gt(visit(ctx.expr(1)));
    }

    /*
     *          Boolean Operations
     */
    @Override
    public ScriptAST visitAnd(nslParser.AndContext ctx) {
        return visit(ctx.expr(0)).and(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitNot(nslParser.NotContext ctx) {
        return visit(ctx.expr()).not();
    }

    @Override
    public ScriptAST visitOr(nslParser.OrContext ctx) {
        return visit(ctx.expr(0)).or(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitImply(nslParser.ImplyContext ctx) {
        return visit(ctx.expr(0)).imply(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitIfte(nslParser.IfteContext ctx) {
        return visit(ctx.expr(0)).ifte(visit(ctx.expr(1)), visit(ctx.expr(2)));
    }


    /*
     *          Functions
     */

    @Override
    public ScriptAST visitFunction(nslParser.FunctionContext ctx) {
        Funzione fun = new Funzione(context, ctx.VARNAME().getText());
        for (nslParser.ExprContext x : ctx.expr()) {
            fun.addExpression(visit(x));
        }
        return new ScriptAST(context, fun);
    }

    @Override
    public ScriptAST visitApply(nslParser.ApplyContext ctx) {
        return visit(ctx.expr(0)).apply(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitInvoke(nslParser.InvokeContext ctx) {
        return visit(ctx.expr(0)).invoke(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitRemove(nslParser.RemoveContext ctx) {
        return visit(ctx.expr(0)).apply(visit(ctx.expr(1)));
    }


    @Override
    public ScriptAST visitAppend(nslParser.AppendContext ctx) {
        return visit(ctx.expr(0)).append(visit(ctx.expr(1)));
    }

    @Override
    public ScriptAST visitPut(nslParser.PutContext ctx) {
        return visit(ctx.expr(0)).put(visit(ctx.expr(1)), visit(ctx.expr(2)));
    }

    /*
     *          String Operations
     */

    @Override
    public ScriptAST visitConcat(nslParser.ConcatContext ctx) {
        return visit(ctx.expr(0)).concat(visit(ctx.expr(1)));
    }


    @Override
    public ScriptAST visitContains(nslParser.ContainsContext ctx) {
        return visit(ctx.expr(1)).contains(visit(ctx.expr(0)));
    }


    @Override
    public ScriptAST visitSubstring(nslParser.SubstringContext ctx) {
        return visit(ctx.expr(0)).substring(visit(ctx.expr(1)), visit(ctx.expr(2)));
    }

    /*
     *          Access
     */
    @Override
    public ScriptAST visitAt(nslParser.AtContext ctx) {
        return visit(ctx.expr(0)).at(visit(ctx.expr(1)));
    }

    /*
     *          Statements
     */
    @Override
    public ScriptAST visitAssign(nslParser.AssignContext ctx) {
        return ScriptAST.assignment(context, visit(ctx.expr(0)), visit(ctx.expr(1)));
    }

    public  void bindObject(AbstractObject toparse) {
        context.put("o", new ScriptAST(context, (Object)toparse));
    }

    public  void bindFunctionAsList(Map toparse) {
        context.put("f", new ScriptAST(context, toparse));
    }

    public void bindFunctionAsList(Iterable toparse) {
        context.put("f", new ScriptAST(context, toparse));
    }

    public  void bindFunctionAsList(Object[] toparse) {
        context.put("f", new ScriptAST(context, toparse));
    }

    public  void bindGSM(GSM g) {
        context.put("g", new ScriptAST(context, g));
    }
}
