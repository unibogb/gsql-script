package it.giacomobergami.nestedmodel2.model.languages.script.lambdas;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.serializable.SerializableFunction;

import java.util.function.Function;

public interface ValueFunction extends Function<ScriptAST, ScriptAST>, SerializableFunction {
}
