package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.functors;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;

import java.util.List;

public class Assign extends ValueBiFunction {
    public static final Assign instance = new Assign(); public Assign() {}
    @Override
    public ScriptAST apply(ScriptAST l, ScriptAST r) {
        r = r.eval();
        if (l.optGamma != null && l.type.equals(ScriptAST.t.Variable)) {
            l.optGamma.put(l.string, r);
        } else if (l.type.equals(ScriptAST.t.Function)) {
            return l.apply(r);
        }
        return r;
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "" + x.get(0).stringSerialize()+" := "+x.get(1).stringSerialize() +"";
    }
}
