package it.giacomobergami.nestedmodel2.model.languages.script.structures;

import it.giacomobergami.nestedmodel2.model.languages.script.annotations.MetaOperation;
import it.giacomobergami.nestedmodel2.model.languages.script.annotations.Statement;

import java.util.ArrayList;
import java.util.Iterator;

public class Funzione {
    public String parameter;
    public ArrayList<ScriptAST> body;
    Gamma externalToUpdate;

    @Override
    public String toString() {
        // Lazy Printing
        StringBuilder sb = new StringBuilder();
        sb.append(parameter).append(" -> {");
        Iterator<ScriptAST> iv = body.iterator();
        while (iv.hasNext()) {
            ScriptAST x = iv.next();
            sb.append(x.stringSerialize());
            if (iv.hasNext()) sb.append("; ");
        }
        sb.append("}");
        return sb.toString();
    }

    public Funzione(Gamma g, String x) {
        externalToUpdate = g;
        parameter = x;
        body = new ArrayList<>();
    }

    public void addExpression(ScriptAST expr) {
        expr.optGamma = externalToUpdate;
        body.add(expr);
    }

    @MetaOperation
    @Statement
    public ScriptAST apply(ScriptAST x) {
        ArrayList<String> ve = new ArrayList<>(externalToUpdate.keySet());
        x = x.eval();
        ScriptAST old = externalToUpdate.put(parameter, x);
        ScriptAST toReturn = new ScriptAST(externalToUpdate, ScriptAST.t.Array);
        for (ScriptAST y : body) {
            y.optGamma = externalToUpdate;
            toReturn = y.run();
        }
        if (old == null) {
            externalToUpdate.remove(parameter);
        } else {
            externalToUpdate.put(parameter, old);
        }
        externalToUpdate.keySet().retainAll(ve);
        return toReturn;
    }

    public void setContext(Gamma context) {
        this.externalToUpdate = context;
        for (int i = 0; i<body.size(); i++)
            body.get(i).setContext(context);
    }
}
