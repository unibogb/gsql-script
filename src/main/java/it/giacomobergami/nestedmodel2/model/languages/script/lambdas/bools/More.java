package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.bools;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;

import java.util.List;

public class More extends BoolBiFunction {
    public static final More instance = new More();
    private More() { }

    @Override
    protected boolean rawApply(ScriptAST l, ScriptAST r) {
        return l.eval().compareTo(r.eval()) > 0;
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "(" + x.get(0).stringSerialize()+") > ("+x.get(1).stringSerialize() +")";
    }
}
