package it.giacomobergami.nestedmodel2.model.languages.GSQL.objects;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.Append;
import it.giacomobergami.nestedmodel2.utils.operators.iterators.IteratorMap;

import java.util.Iterator;

public class DisjointUnionObject extends AbstractObject {
    private final JId id;
    private final AbstractObject left, right;
    private final GSM container;

    public DisjointUnionObject(GSM container, JId id, AbstractObject one, AbstractObject two) {
        this.id = id;
        this.left = one;
        this.right = two;
        this.container = container;
    }

    @Override
    public JId id() {
        return id;
    }

    @Override
    public Iterator getEllIterator() {
        return new Append<>(left.getEllIterator(), right.getEllIterator());
    }

    @Override
    public Iterator<String> getXiIterator() {
        return new Append<>(left.getXiIterator(), right.getXiIterator());
    }

    @Override
    public Iterator<JPair<String, Iterator<JId>>> getPhiIterator() {
        Iterator<JPair<String, Iterator<JId>>> leftMap =
                new IteratorMap<JPair<String, Iterator<JId>>, JPair<String, Iterator<JId>>>(left.getPhiIterator()) {
                    @Override
                    public JPair<String, Iterator<JId>> apply(JPair<String, Iterator<JId>> expressionIteratorJPair) {
                        return new JPair<>("[1, "+ expressionIteratorJPair.key+"]", expressionIteratorJPair.value);
                    }
                };
        Iterator<JPair<String, Iterator<JId>>> rightMap =
                new IteratorMap<JPair<String, Iterator<JId>>, JPair<String, Iterator<JId>>>(right.getPhiIterator()) {
                    @Override
                    public JPair<String, Iterator<JId>> apply(JPair<String, Iterator<JId>> expressionIteratorJPair) {
                        return new JPair<>("[2, "+ expressionIteratorJPair.key+"]", expressionIteratorJPair.value);
                    }
                };
        return new Append<>(leftMap, rightMap);
    }

    @Override
    public GSM getContainer() {
        return container;
    }

}
