package it.giacomobergami.nestedmodel2.model.languages.GSQL;

import java.util.Iterator;
import java.util.function.BiFunction;

/**
 * Implements the high order operator
 * @param <T>
 * @param <K>
 */
public class Fold<T,K> {
    private final Iterator<T> S;
    private final BiFunction<T,K, K> f;

    public Fold(Iterator<T> set, BiFunction<T,K,K> transformation) {
        S = set;
        this.f = transformation;
    }

    public K apply(K accumulator) {
        K curr = accumulator;
        while (S.hasNext()) {
            curr = f.apply(S.next(), curr);
        }
        return curr;
    }
}
