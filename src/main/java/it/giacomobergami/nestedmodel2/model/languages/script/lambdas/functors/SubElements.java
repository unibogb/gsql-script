package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.functors;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueTerFunction;

import java.util.List;

public class SubElements extends ValueTerFunction {
    public final static ValueTerFunction instance = new SubElements(); SubElements() {}

    @Override
    public ScriptAST apply(ScriptAST left, ScriptAST middle, ScriptAST right) {
        int min = (int)middle.toInteger().toLong();
        int max = (int)right.toInteger().toLong();
        switch (left.type) {
            case String:
                return ScriptAST.string(left.optGamma, left.string.substring(min, max));
            default:
                return new ScriptAST(left.optGamma, left.toList().subList(min, max));
        }
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "substring(" + x.get(0).stringSerialize()+", "+x.get(1).stringSerialize()+", "+x.get(2).stringSerialize() +")";
    }
}
