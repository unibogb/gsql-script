package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.bools;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueFunction;

public abstract class BoolFunction implements ValueFunction {
    protected abstract boolean rawApply(ScriptAST left);
    @Override
    public ScriptAST apply(ScriptAST left) {
        return new ScriptAST(left.optGamma, rawApply(left));
    }
}