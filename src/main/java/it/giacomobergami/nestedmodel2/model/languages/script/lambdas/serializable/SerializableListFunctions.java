package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.serializable;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;

import java.util.List;
import java.util.function.Function;

public abstract class SerializableListFunctions implements Function<List<ScriptAST>, ScriptAST>, SerializableFunction {
}
