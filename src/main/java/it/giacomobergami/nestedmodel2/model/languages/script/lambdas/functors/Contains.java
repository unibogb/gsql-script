package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.functors;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;

import java.util.ArrayList;
import java.util.List;

public class Contains extends ValueBiFunction {
    public final static Contains instance = new Contains(); Contains() {}
    @Override
    public ScriptAST apply(ScriptAST dis, ScriptAST value) {
        switch (value.type) {
            case Bool:
            case Integer:
            case Double:
            case String:
                return dis.eq(value);// Slight enhancement
            case Java:
            case JavaMethod:
            case Variable:
            case LazyExpression:
                return value.evalPriorToApplication(y -> y.apply(dis));
        }
        ArrayList<ScriptAST> x = dis.toList();
        return new ScriptAST(dis.optGamma, x.contains(value));
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "" + x.get(0).stringSerialize()+" in "+x.get(1).stringSerialize() +"";
    }
}
