package it.giacomobergami.nestedmodel2.model.languages.script;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptVisitor;
import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.model.logical.scriptlanguage.nslLexer;
import it.giacomobergami.nestedmodel2.model.logical.scriptlanguage.nslParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.Map;

public class ScriptLanguage {

    private ScriptVisitor visitor;
    private ParseTree tree;

    public ScriptLanguage(String toParse) {
        Lexer lexer = new nslLexer(CharStreams.fromString(toParse));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        nslParser parser = new nslParser(tokens);
         tree = parser.nsl();
        visitor = new ScriptVisitor();
    }

    public ScriptLanguage setObjectO(AbstractObject object) {
        visitor.bindObject(object);
        return this;
    }

    public ScriptLanguage setFunctionF(Iterable object) {
        visitor.bindFunctionAsList(object);
        return this;
    }

    public ScriptLanguage setFunctionF(Map object) {
        visitor.bindFunctionAsList(object);
        return this;
    }

    public ScriptAST evaluateWithContext(AbstractObject object) {
        visitor.bindObject(object);
        return visitor.visit(tree);
    }

    public ScriptAST evaluate() {
        return visitor.visit(tree);
    }

    public ScriptLanguage setGSM(GSM GSM) {
        visitor.bindGSM(GSM);
        return this;
    }
}
