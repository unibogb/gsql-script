package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.expressions;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class At extends ValueBiFunction {
    public static ValueBiFunction instance = new At(); At() {}

    @Override
    public ScriptAST apply(ScriptAST dis, ScriptAST position) {
        switch (dis.type) {
            case String:
                return ScriptAST.string(dis.optGamma, dis.toString().charAt((int) position.toInteger().toLong()) + "");
            case JavaMethod:
            case Variable:
            case LazyExpression: {
                return dis.evalPriorToApplication(x -> x.at(position));
            }
            case Java: {
                if (dis.object.reference instanceof AbstractObject.PhiMap) {
                    return new ScriptAST(dis.optGamma, ((AbstractObject.PhiMap) dis.object.reference).get(position.toString()));
                } else if (dis.object.reference instanceof Map) {
                    return new ScriptAST(dis.optGamma, ((Map) dis.object.reference).get(position.toString()));
                }
            }
            default: {
                ArrayList<ScriptAST> z = dis.toList();
                int pos = (int) position.toInteger().toLong();
                if (z.size() > pos && pos >= 0) {
                    return z.get(pos);
                } else return new ScriptAST(dis.optGamma);
            }
        }
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "(" + x.get(0).stringSerialize()+")["+x.get(1).stringSerialize() +"]";
    }
}
