package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.bools;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueFunction;

import java.util.List;

public class Not extends  BoolFunction{
    public static final ValueFunction instance = new Not();
    Not () {}

    @Override
    protected boolean rawApply(ScriptAST left) {
        return !left.toBoolean();
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "not (" + x.get(0).stringSerialize()+")";
    }
}
