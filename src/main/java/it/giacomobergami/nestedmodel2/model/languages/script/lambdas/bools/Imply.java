package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.bools;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;

import java.util.List;

public class Imply extends ValueBiFunction {
    public static final Imply instance = new Imply();
    Imply() {}
    @Override
    public ScriptAST apply(ScriptAST z, ScriptAST y) {
        return z.toBoolean() ? z : y;
    }

    @Override
    public String toString(List<ScriptAST> x) {
        return "(" + x.get(0).stringSerialize() + ") => (" + x.get(1).stringSerialize() + ")";
    }
}
