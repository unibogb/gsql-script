package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.expressions;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueTerFunction;

import java.util.ArrayList;
import java.util.List;

public class Put extends ValueTerFunction {
    public static final Put instance = new Put(); public Put() {}
    @Override
    public ScriptAST apply(ScriptAST dis, ScriptAST position, ScriptAST newValue) {
        if (dis.type.equals(ScriptAST.t.Java)) {
            if (dis.object.reference instanceof AbstractObject.PhiMap) {
                return new ScriptAST(dis.optGamma, ((AbstractObject.PhiMap) dis.object.reference).get(position.toString()));
            }
        }

        ArrayList<ScriptAST> disList = dis.toList();
        int disListPos = (int) position.toInteger().toLong();

        ScriptAST toret;
        if (disListPos >= 0) {
            disList.add(disListPos, newValue);
            toret = newValue;                   //Some Return
        } else
            toret = new ScriptAST(dis.optGamma); //Empty return

        ///???
        if (dis.optGamma != null) {
            dis.optGamma.put(dis.string != null ? dis.string : dis.toString(), dis);
        }
        return toret;
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "(" + x.get(0).stringSerialize()+")["+x.get(1).stringSerialize() +"]:=" + x.get(2).stringSerialize();
    }
}
