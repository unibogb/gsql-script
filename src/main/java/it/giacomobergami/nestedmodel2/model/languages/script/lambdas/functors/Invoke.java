package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.functors;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.Javification;
import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;

import java.util.List;

public class Invoke extends ValueBiFunction {
    public static final Invoke instance = new Invoke(); Invoke() {}
    @Override
    public ScriptAST apply(ScriptAST l, ScriptAST r) {
        switch (l.type) {
            case Integer:
            case Bool:
            case Double:
            case String:
            case Array:
                return new Javification(l.optGamma, l.toJavaObject()).invoke(r);
            case Function:
                return l;
            case Java:
                return l.object.invoke(r.toString());
            case Assignment:
            case JavaMethod:
            case LazyExpression:
            case Variable:
                return l.evalPriorToApplication(x -> x.invoke(r));
        }
        return new ScriptAST(l.optGamma);
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "(" + x.get(0).stringSerialize()+") . ("+x.get(1).stringSerialize() +")";
    }
}
