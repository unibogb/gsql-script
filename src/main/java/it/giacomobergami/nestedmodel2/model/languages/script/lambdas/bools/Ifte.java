package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.bools;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueTerFunction;

import java.util.List;

public class Ifte extends ValueTerFunction {
    public static final ValueTerFunction instance = new Ifte();
    Ifte() {}

    @Override
    public ScriptAST apply(ScriptAST left, ScriptAST middle, ScriptAST right) {
        return left.toBoolean() ? middle : right;
    }

    @Override
    public String toString(List<ScriptAST> x) {
        return "if (" + x.get(0).stringSerialize() + ") then (" + x.get(1).stringSerialize() + ") else (" + x.get(2).stringSerialize() + ")";
    }
}
