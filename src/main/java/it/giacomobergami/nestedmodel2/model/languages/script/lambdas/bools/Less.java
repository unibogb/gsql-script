package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.bools;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;

import java.util.List;

public class Less extends BoolBiFunction {

    public static final Less instance = new Less();
    private Less() { }

    @Override
    protected boolean rawApply(ScriptAST l, ScriptAST r) {
        return l.eval().compareTo(r.eval()) < 0;
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return "(" + x.get(0).stringSerialize()+") < ("+x.get(1).stringSerialize() +")";
    }
}
