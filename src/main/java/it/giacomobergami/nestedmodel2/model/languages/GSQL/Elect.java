package it.giacomobergami.nestedmodel2.model.languages.GSQL;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.utils.JId;

public class Elect {

    public static GSM perform(GSM element, JId referenceObject) {
        if (element.O.contains(referenceObject)) {
            return new GSM(element.currentStep, element.O, referenceObject);
        } else {
            return element;
        }
    }

}
