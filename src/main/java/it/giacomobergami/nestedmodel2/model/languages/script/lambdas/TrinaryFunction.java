package it.giacomobergami.nestedmodel2.model.languages.script.lambdas;

@FunctionalInterface
public interface TrinaryFunction<X,Y,Z,T> {
    T apply(X arg0, Y arg1, Z arg2);
    default  String opRepresentation() {
        return "";
    }
}
