package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.numbers;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;
import it.giacomobergami.nestedmodel2.utils.JId;

import java.util.ArrayList;

public abstract class NumberBiFunction extends ValueBiFunction{
    protected abstract double doubleFunction(double left, double right);
    protected abstract JId idFunction(JId left, JId right);
    @Override
    public ScriptAST apply(ScriptAST z, ScriptAST x) {
        //System.out.println(this.toString(new ArrayList<>())+" "+z.argsWithFunction.key.get(0).type+" "+z.type+" "+x.type);
        return (z.isDouble() || x.isDouble()) ?
                new ScriptAST(z.optGamma, doubleFunction(z.toDouble(), x.toDouble())) :
                new ScriptAST(z.optGamma, idFunction(z.toInteger(),(x.toInteger())));
    }
}
