package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.string;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;

import java.util.List;

public class Concat extends StringBiFunction{
    public static final Concat instance = new Concat(); Concat() {}
    @Override
    protected String map(String left, String right) {
        return left+right;
    }

    @Override
    public String toString(List<ScriptAST> x) {
        return "(" + x.get(0).stringSerialize()+") ++ ("+x.get(1).stringSerialize() +")";
    }
}
