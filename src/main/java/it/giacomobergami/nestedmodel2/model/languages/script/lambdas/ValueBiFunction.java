package it.giacomobergami.nestedmodel2.model.languages.script.lambdas;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.serializable.SerializableFunction;

import java.util.function.BiFunction;

public abstract class ValueBiFunction implements BiFunction<ScriptAST, ScriptAST, ScriptAST>, SerializableFunction {
    @Override
    public abstract ScriptAST apply(ScriptAST left, ScriptAST right);
}