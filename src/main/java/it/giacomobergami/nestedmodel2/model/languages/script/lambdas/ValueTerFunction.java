package it.giacomobergami.nestedmodel2.model.languages.script.lambdas;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.serializable.SerializableFunction;

public abstract class ValueTerFunction implements TrinaryFunction<ScriptAST, ScriptAST, ScriptAST, ScriptAST>, SerializableFunction {
    @Override
    public abstract ScriptAST apply(ScriptAST left, ScriptAST middle, ScriptAST right);
}