package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.numbers;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.utils.JId;

import java.util.List;

public class Add extends NumberBiFunction{
    public static final Add instance = new Add(); Add() {}
    @Override
    protected double doubleFunction(double left, double right) {
        return left+right;
    }
    @Override
    protected JId idFunction(JId left, JId right) {
        return left.add(right);
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return x.size() == 0 ? "!+" : "(" + x.get(0).stringSerialize()+") + ("+x.get(1).stringSerialize() +")";
    }
}
