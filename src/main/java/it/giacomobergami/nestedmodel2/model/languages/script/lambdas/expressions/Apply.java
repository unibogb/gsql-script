package it.giacomobergami.nestedmodel2.model.languages.script.lambdas.expressions;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;
import it.giacomobergami.nestedmodel2.model.languages.script.lambdas.ValueBiFunction;

import java.util.List;

public class Apply extends ValueBiFunction {
    public final static ValueBiFunction instance = new Apply(); Apply() {}

    @Override
    public ScriptAST apply(ScriptAST left, ScriptAST right) {
        //System.out.println("Apply of " + left.type);
        switch (left.type) {
            case Variable:
                return left.optGamma.get(left.string).apply(right);
            case Function:
                return left.function.apply(right);
            default:
                return left.eval();
        }
    }


    @Override
    public String toString(List<ScriptAST> x) {
        return apply(x.get(0), x.get(1)).stringSerialize();
    }
}
