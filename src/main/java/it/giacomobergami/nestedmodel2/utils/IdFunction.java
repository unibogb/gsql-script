package it.giacomobergami.nestedmodel2.utils;

import it.giacomobergami.nestedmodel2.model.languages.script.structures.ScriptAST;

import java.util.function.Function;

public class IdFunction<T> implements Function<T, T> {

    public static final Function<ScriptAST,ScriptAST> valuesInstance = new IdFunction<>();

    @Override
    public T apply(T t) {
        return t;
    }
}
