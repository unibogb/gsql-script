package it.giacomobergami.nestedmodel2.utils.operators.iterators;

import java.util.Iterator;

public class AppendSingle<T> implements Iterator<T> {
    private final Iterator<T> basis;
    private final T elem;
    private boolean visitedHead;

    public AppendSingle(T elem, Iterator<T> basis) {
        this.basis = basis;
        this.elem = elem;
        visitedHead = false;
    }


    @Override
    public boolean hasNext() {
        return (!visitedHead) || basis.hasNext();
    }

    @Override
    public T next() {
        if (!visitedHead) {
            visitedHead = true;
            return elem;
        } else {
            return basis.next();
        }
    }
}
