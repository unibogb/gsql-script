package it.giacomobergami.nestedmodel2.utils.operators.iterators;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Arrays; /* For sample code */
import java.util.List;

public class Append<T> implements Iterator<T> {
    private final List<Iterator<T>> is;
    private int current;

    public Append(Iterator<T>... iterators)
    {
            is = Arrays.asList(iterators);
            current = 0;
    }

    public Append(ArrayList<Iterator<T>> iterators)
    {
        is = iterators;
        current = 0;
    }

    public boolean hasNext() {
            while ( current < is.size() && !is.get(current).hasNext() )
                    current++;

            return current < is.size();
    }

    public T next() {
            while ( current < is.size() && !is.get(current).hasNext() )
                    current++;

            return is.get(current).next();
    }
}