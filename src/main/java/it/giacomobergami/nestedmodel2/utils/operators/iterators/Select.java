package it.giacomobergami.nestedmodel2.utils.operators.iterators;

import java.util.Iterator;
import java.util.function.Predicate;

public abstract class Select<T> implements Iterator<T>, Predicate<T> {

    private final Iterator<T> it;
    private T current;

    public Select(Iterator<T> it) {
        this.it = it;
        current = null;
    }

    @Override
    public boolean hasNext() {
        if (current == null || (!test(current))) {
            while (it.hasNext()) {
                current = it.next();
                if (test(current)) return true;
            }
            if (!it.hasNext())
                current = null;
        }
        return current != null;
    }

    @Override
    public T next() {
        T toret = current;
        current = (it.hasNext()) ? it.next() : null;
        return toret;
    }

}
