package it.giacomobergami.nestedmodel2.utils.operators.objectSet;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.AbstractObject;
import it.giacomobergami.nestedmodel2.utils.CompareOptional;
import it.giacomobergami.nestedmodel2.utils.JId;
import it.giacomobergami.nestedmodel2.utils.JPair;
import it.giacomobergami.nestedmodel2.utils.operators.UnsafeOperation;
import it.giacomobergami.nestedmodel2.model.logical.object.model.objectset.ConcreteObjectSet;
import it.giacomobergami.nestedmodel2.model.logical.object.model.objectset.ObjectSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Optional;

@UnsafeOperation
public class Append implements ObjectSet {
    private final ObjectSet  is[];
    private final ConcreteObjectSet overflow;

    private Append(ObjectSet... is) {
        this.is = is;
        overflow = new ConcreteObjectSet();
    }

    public static  Append append(ObjectSet... is) {
        if (is.length == 1 && is[0] instanceof Append) {
            Append a = (Append )is[0];
            Append toret = new Append(a.is);
            toret.overflow.putAll(a.overflow);
            return toret;
        } else {
            return new Append(is);
        }
    }

    @Override
    public AbstractObject put(JId key, AbstractObject value) {
        return overflow.put(key, value);
    }

    @Override
    public AbstractObject get(JId key) {
        AbstractObject toret = overflow.get(key);
        if (toret == null) {
            for (ObjectSet k : is) {
                if ((toret = k.get(key))!=null)
                    return toret;
            }
        }
        return toret;
    }

    @Override
    public Optional<JId> minKey() {
        Optional<JId> min = Arrays.stream(is)
                .map(ObjectSet::minKey)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .min(JId::compareTo);
        return CompareOptional.min(min, overflow.minKey());
    }

    @Override
    public Optional<JId> maxKey() {
        Optional<JId> max = Arrays.stream(is)
                .map(ObjectSet::minKey)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .max(JId::compareTo);
        return CompareOptional.max(max, overflow.maxKey());
    }

    @Override
    public boolean isEmpty() {
        boolean empty = true;
        for (int i = 0; i<is.length; i++) {
            empty = empty && is[i].isEmpty();
            if (!empty) return false;
        }
        return empty && overflow.isEmpty();
    }

    @Override
    public boolean contains(JId key) {
        if (!overflow.contains(key)) {
            for (ObjectSet k : is) {
                if (k.contains(key))
                    return true;
            }
            return false;
        } else
            return true;
    }

    @Override
    public void setOwner(GSM labelExprGSM) {
        throw new RuntimeException("Unexpected assignment: setOwner in Append AbstractObject");
    }

    @Override
    public Iterator<JPair<JId, AbstractObject>> iterator() {
        ArrayList<Iterator<JPair<JId, AbstractObject>>> tail = new ArrayList<>(is.length+1);
        tail.add(overflow.iterator());
        for (int i = 0; i< is.length; i++)
            tail.add(is[i].iterator());
        return new it.giacomobergami.nestedmodel2.utils.operators.iterators.Append<>(tail);
    }
}
