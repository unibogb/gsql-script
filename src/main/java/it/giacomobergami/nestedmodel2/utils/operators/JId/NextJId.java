package it.giacomobergami.nestedmodel2.utils.operators.JId;

import it.giacomobergami.nestedmodel2.utils.JId;

import java.util.function.Function;

public class NextJId implements Function<JId, JId> {

    private NextJId() {

    }
    @Override
    public JId apply(JId jId) {
        return jId.add(1).add(jId);
    }

    public static final NextJId instance = new NextJId();
}
