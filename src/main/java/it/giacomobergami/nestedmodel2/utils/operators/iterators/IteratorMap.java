package it.giacomobergami.nestedmodel2.utils.operators.iterators;

import java.util.Iterator;
import java.util.function.Function;

public abstract class IteratorMap<T,D> implements Iterator<D>, Function<T,D> {
    private final Iterator<T> iterator;
    public IteratorMap(Iterator<T> iterator) {
        this.iterator = iterator;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public D next() {
        return apply(iterator.next());
    }
}
