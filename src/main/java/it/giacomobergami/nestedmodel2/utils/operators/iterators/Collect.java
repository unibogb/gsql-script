package it.giacomobergami.nestedmodel2.utils.operators.iterators;

import java.util.ArrayList;
import java.util.Iterator;

public class Collect<T> extends ArrayList<T> {
    public Collect(Iterator<T> it) {
        super();
        it.forEachRemaining(this::add);
    }
}
