package it.giacomobergami.nestedmodel2.utils.operators.iterators;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Remove<T> extends Select<T> {

    private final Set<T> toremove = new HashSet<>();

    public Remove(Iterator<T> it, T... torem) {
        super(it);
        for (T x : torem) {
            toremove.add(x);
        }
    }

    public Remove(Iterator<T> it, Collection<T> torem) {
        super(it);
        toremove.addAll(torem);
    }

    @Override
    public boolean test(T t) {
        return !toremove.contains(t);
    }
}
