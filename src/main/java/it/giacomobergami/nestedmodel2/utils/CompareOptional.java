package it.giacomobergami.nestedmodel2.utils;

import javax.swing.text.html.Option;
import java.util.Comparator;
import java.util.Optional;

public class CompareOptional {


    public static <T extends Comparable<T>> T min(T left, T right) {
        return left.compareTo(right) < 0 ? left : right;
    }

    public static <T extends Comparable<T>> T max(T left, T right) {
        return left.compareTo(right) > 0 ? left : right;
    }

    public static <T extends Comparable<T>> Optional<T> min(Optional<T> left, Optional<T> right) {
        if (left.isPresent()) {
            return Optional.of(right.map(t -> min(left.get(), t)).orElseGet(left::get));
        } else if (right.isPresent()) {
            return Optional.of(right.get());
        }
        return Optional.empty();
    }

    public static <T extends Comparable<T>> Optional<T> max(Optional<T> left, Optional<T> right) {
        if (left.isPresent()) {
            return Optional.of(right.map(t -> max(left.get(), t)).orElseGet(left::get));
        } else if (right.isPresent()) {
            return Optional.of(right.get());
        }
        return Optional.empty();
    }



}
