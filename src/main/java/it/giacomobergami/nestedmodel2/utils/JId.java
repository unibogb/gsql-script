package it.giacomobergami.nestedmodel2.utils;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.lang.reflect.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

import static java.math.BigDecimal.ROUND_HALF_UP;

public class JId implements Comparable<JId> {

    public final static IdClassInformation internals;
    public final static JId ONE;
    public final static JId ZERO;
    public final static JId TEN;
    public final static JId TWO;

    static {
        internals = IdClassInformation.idsConstructor();
        ONE = new JId(1);
        ZERO = new JId(0);
        TEN = new JId(10);
        TWO = new JId(2);
    }

    // Constructors
    public JId() {
        this(0);
    }


    public JId(BigInteger r) {
        this.representation = internals.generate(r);
    }

    protected JId(Object r) {
        this.representation = r;
    }

    public JId(JId copy) {
        this.representation = internals.generate(copy.representation);
    }

    public JId(int start) {
        representation = internals.generate(start);
    }


    public JId(int[] deserialize) {
        representation = internals.deserialize(deserialize);
    }

    public long toLong() {
        return internals.toLong(representation);
    }

    public JId dovetailWithZero() {
        return dovetail(ZERO, this);
    }

    public static class IdClassInformation {
        private final Constructor<?> conBI;
        private final Method normalize;
        private final Method toBI;
        Class<?> c;
        final Constructor<?> con, conSelf, conArray;
        private final Method multiplyInteger, multiply, cmp, add, difference, dKnuth, tolong;
        private static IdClassInformation self = null;
        private Field array_value;

        private IdClassInformation() throws ClassNotFoundException, NoSuchMethodException, NoSuchFieldException {
            c = Class.forName("java.math.MutableBigInteger");
            con = c.getDeclaredConstructor(int.class);
            con.setAccessible(true);
            conSelf = c.getDeclaredConstructor(c);
            conSelf.setAccessible(true);
            conBI = c.getDeclaredConstructor(BigInteger.class);
            conBI.setAccessible(true);
            conArray = c.getDeclaredConstructor(int[].class);
            conArray.setAccessible(true);
            multiplyInteger = c.getDeclaredMethod("mul", new Class[] { int.class, c });
            multiplyInteger.setAccessible(true);
            multiply = c.getDeclaredMethod("multiply", new Class[] { c, c });
            multiply.setAccessible(true);
            cmp = c.getDeclaredMethod("compare", new Class[]{c});
            cmp.setAccessible(true);
            add = c.getDeclaredMethod("add", new Class[]{c});
            add.setAccessible(true);
            difference = c.getDeclaredMethod("difference", new Class[]{c});
            difference.setAccessible(true);
            dKnuth = c.getDeclaredMethod("divide", new Class[] { c, c, boolean.class });
            dKnuth.setAccessible(true);
            tolong = c.getDeclaredMethod("toCompactValue", new Class[]{int.class});
            tolong.setAccessible(true);
            array_value = c.getDeclaredField("value");
            array_value.setAccessible(true);
            normalize = c.getDeclaredMethod("normalize");
            normalize.setAccessible(true);
            toBI = c.getDeclaredMethod("toBigInteger", int.class);
            toBI.setAccessible(true);
        }

        public BigInteger toBigInteger(Object self) {
            try {
                return (BigInteger) toBI.invoke(self, 1);
            } catch (IllegalAccessException | InvocationTargetException e) {
                return BigInteger.ZERO;
            }
        }

        public long toLong(Object longify) {
            try {
                return (long) tolong.invoke(longify, 1);
            } catch (IllegalAccessException | InvocationTargetException e) {
                return -1;
            }
        }

        public static IdClassInformation idsConstructor() {
            if (self == null) try {
                self = new IdClassInformation();
            } catch (ClassNotFoundException | NoSuchMethodException | NoSuchFieldException e) {
                self = null;
            }
            return self;
        }

        public int hashCode(Object toHash) {
            int var1 = 0;
            Object array;
            try {
                array = array_value.get(toHash);
            } catch (IllegalAccessException e) {
                return  0;
            }
            int len = Array.getLength(array);
            for(int var2 = 0; var2 < len; ++var2) {
                Number n = (Number)Array.get(array, var2);
                var1 = (int)(31L * var1 + (n.longValue() & 4294967295L));
            }
            return var1;
        }

        public Object generate(int basic) {
            try {
                return con.newInstance(basic);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                return null;
            }
        }

        public Object generate(BigInteger basic) {
            try {
                return conBI.newInstance(basic);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                return null;
            }
        }

        public Object generate(Object basic) {
            try {
                return conSelf.newInstance(basic);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                return null;
            }
        }

        public Object deserialize(int[] self) {
            try {
                return conArray.newInstance(self);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                return null;
            }
        }

        public Object times(int basic, Object fromid) {
            try {
                Object result = generate(0);
                multiplyInteger.invoke(fromid, basic, result);
                return result;
            } catch (IllegalAccessException | InvocationTargetException e) {
                return null;
            }
        }

        public Object division(Object dividendo, Object divisore) {
            try {
                Object result = ZERO.copy().representation;
                this.dKnuth.invoke(dividendo, divisore, result, false);
                return result;
            } catch (IllegalAccessException | InvocationTargetException e) {
                return null;
            }
        }

        public Object times(Object left, Object right) {
            try {
                Object result = generate(0);
                multiply.invoke(left, right, result);
                return result;
            } catch (IllegalAccessException | InvocationTargetException e) {
                return null;
            }
        }

        private int cmp(Object m1, Object m2) {
            //int[] m1 = mag;
            int len1 = Array.getLength(m1);
            //int[] m2 = val;
            int len2 = Array.getLength(m2);
            if (len1 < len2)
                return -1;
            if (len1 > len2)
                return 1;
            for (int i = 0; i < len1; i++) {
                int a = (int)Array.get(m1, i);
                int b = (int)Array.get(m2, i);
                if (a != b)
                    return ((a & 0xffffffffL) < (b & 0xffffffffL)) ? -1 : 1;
            }
            return 0;
        }

        public int compare(Object left, Object right) throws InvocationTargetException, IllegalAccessException {
            return (int)cmp(array_value.get(left), array_value.get(right));
        }

        public boolean plus(Object left, Object right) {
            try {
                add.invoke(left, right);
                return true;
            } catch (IllegalAccessException | InvocationTargetException e) {
                return false;
            }
        }

        public boolean minus(Object left, Object right) {
            try {
                difference.invoke(left, right);
                return true;
            } catch (IllegalAccessException | InvocationTargetException e) {
                return false;
            }
        }

        public int[] asIntArray(Object self) {
            try {
                return (int[])(array_value.get(self));
            } catch (IllegalAccessException e) {
                return null;
            }
        }
    }

    private Object representation;

    public JId multiply(int basic) {
        return new JId(internals.times(basic, representation));
    }

    public JId multiply(JId left) {
        return new JId(internals.times(left.representation, representation));
    }

    public JId add(JId right) {
        JId x = this.copy();
        x.internals.plus(x.representation, right.representation);
        return x;
    }

    public JId add(int right) {
        JId x = this.copy();
        x.internals.plus(x.representation, internals.generate(right));
        return x;
    }

    public JId subtract(JId right) {
        JId x = this.copy();
        x.internals.minus(x.representation, right.representation);
        return x;
    }

    public JId subtract(int right) {
        JId x = this.copy();
        x.internals.minus(x.representation, internals.generate(right));
        return x;
    }

    public JId divideBy(int divisor) {
        return new JId(internals.division(representation, internals.generate(divisor)));
    }

    public JId divideBy(JId divisor) {
        return new JId(internals.division(representation, divisor.representation));
    }

    public JId copy() {
        int[] src = this.serialize();
        int[] dest = new int[src.length];
        System.arraycopy( src, 0, dest, 0, src.length );
        return new JId(dest);
    }

    public int[] serialize() {
        return internals.asIntArray(representation);
    }

    public static JId sqRootTwoFloor(JId x) throws IllegalArgumentException {
        if (x.compareTo(ZERO) < 0) {
            throw new IllegalArgumentException("Negative argument.");
        }
        // square roots of 0 and 1 are trivial and
        // y == 0 will cause a divide-by-zero exception
        if (x.equals(ZERO) || x.equals(ONE)) {
            return x;
        } // end if
        JId two = TWO;
        JId y;
        // starting with y = x / 2 avoids magnitude issues with x squared
        for (y = x.divideBy(two); y.compareTo(x.divideBy(y)) > 0; y = ((x.divideBy(y)).add(y)).divideBy(two));
        return y;
    }

    public static JId dovetail(JId i, JId j) {
        JId ij = i.copy().add(j);
        JId ij1  = ij.copy().add(JId.ONE);
        return ij1.multiply(ij).divideBy(2).add(j);
    }

    public static JId bigIntSqRootCeil(JId x) throws IllegalArgumentException {
        if (x.compareTo(JId.ZERO) < 0) {
            throw new IllegalArgumentException("Negative argument.");
        }
        // square roots of 0 and 1 are trivial and
        // y == 0 will cause a divide-by-zero exception
        if (x == JId.ZERO || x == JId.ONE) {
            return x;
        } // end if
        JId two = TWO;
        JId y;
        // starting with y = x / 2 avoids magnitude issues with x squared
        for (y = x.divideBy(two);
             y.compareTo(x.divideBy(y)) > 0;
             y = ((x.divideBy(y)).add(y)).divideBy(two));
        if (x.compareTo(y.multiply(y)) == 0) {
            return y;
        } else {
            return y.add(JId.ONE);
        }
    }

    public static JId generateFromList(ArrayList<JId> list) {
        if (list.isEmpty()) {
            return new JId(0);
        } else if (list.size() == 1) {
            return JId.dovetail(JId.ONE, list.iterator().next());
        } else {
            int size = list.size();
            JId base = JId.dovetail(list.get(size-2), list.get(size-1));
            for (int j = list.size()-3; j >= 0; j--) {
                base = JId.dovetail(list.get(j), base);
            }
            return JId.dovetail(new JId(list.size()), base);
        }
    }

    private static final BigDecimal bd2 = BigDecimal.valueOf(2);
    private static final BigInteger bi2 = BigInteger.valueOf(2);
    private static final int DEFAULT_SCALE = 10;
    public static BigDecimal sqrt(BigDecimal A, final int SCALE) {
        BigDecimal x0 = new BigDecimal("0");
        BigDecimal x1 = new BigDecimal(Math.sqrt(A.doubleValue()));
        while (!x0.equals(x1)) {
            x0 = x1;
            x1 = A.divide(x0, SCALE, BigDecimal.ROUND_HALF_UP);
            x1 = x1.add(x0);
            x1 = x1.divide(bd2, SCALE, BigDecimal.ROUND_HALF_UP);

        }
        return x1;
    }

    public BigDecimal squareRootDecimal() {
        BigDecimal bd = new BigDecimal(internals.toBigInteger(representation));
        return sqrt(bd, DEFAULT_SCALE);
        /*// square roots of 0 and 1 are trivial and
        // y == 0 will cause a divide-by-zero exception
        if (equals(JId.ZERO) ||equals(JId.ONE)) {
            return this.copy();
        } // end if
        JId two = JId.TWO.copy();
        JId y;
        // starting with y = x / 2 avoids magnitude issues with x squared
        for (y = this.divideBy(two);
             y.compareTo(divideBy(y)) > 0;
             y = ((divideBy(y)).add(y)).divideBy(two));
        return y;*/
    }

    @Override
    public String toString() {
        return representation.toString();
    }

    @Override
    public int compareTo(JId id) {
        try {
            int res = Long.compare(toLong(), id.toLong());
            return res == 0 ? res : internals.compare(representation, id.representation);
        } catch (InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public JPair<JId, JId> undovetail() {
        return undovetail(this);
    }

    public static JPair<JId, JId> undovetail(JId c) {

        //JId dmax_ = new JId(c.multiply(8).add(1).squareRootDecimal().subtract(BigDecimal.ONE).divide(bd2).toBigInteger());
        BigInteger dmax = c.multiply(8).add(1).squareRootDecimal().subtract(BigDecimal.ONE).divide(bd2).toBigInteger();
        BigInteger sdmax = dmax.add(BigInteger.ONE).multiply(dmax).divide(bi2);
        BigInteger right = internals.toBigInteger(c.representation).subtract(sdmax);
        BigInteger left = dmax.subtract(right);
        return new JPair<>(new JId(left), new JId(right));
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof JId && compareTo((JId) o) == 0;
    }

    @Override
    public int hashCode() {
        return internals.hashCode(representation);
    }

}