package it.giacomobergami.nestedmodel2.model.languages.GSQL;

import it.giacomobergami.nestedmodel2.model.logical.object.model.gsm.GSM;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.ObjectModel;
import it.giacomobergami.nestedmodel2.model.logical.object.model.objectset.ConcreteObjectSet;
import it.giacomobergami.nestedmodel2.utils.JId;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;

public class MapTest extends TestCase {

    @Test
    public void testEll() {
        GSM gsm = new GSM(1, new ConcreteObjectSet(), new JId(1));

        ObjectModel tmp = new ObjectModel(null, new JId(1));
        tmp._ell.add("hellos");
        tmp._xi.add(("hello"));
        tmp._phi.put(("s"), new ArrayList() {{add(new JId(2)); add(new JId(3));}});
        gsm.create(new JId(1),tmp._ell,tmp._xi,tmp._phi);

        tmp = new ObjectModel(null, new JId(2));
        tmp._ell.add("hellos2");
        tmp._xi.add(("hello"));
        tmp._xi.add(("ua"));
        tmp._phi.put(("s"), new ArrayList() {{add(new JId(3)); }});
        gsm.create(new JId(2),tmp._ell,tmp._xi,tmp._phi);

        tmp = new ObjectModel(null, new JId(3));
        tmp._ell.add("hellos3");
        tmp._xi.add(("hello"));
        tmp._xi.add(("ba"));
        tmp._phi.put(("s"), new ArrayList<>());
        gsm.create(new JId(3),tmp._ell,tmp._xi,tmp._phi);

        gsm.O.forEach(System.out::println);
        System.out.println("\n\n");

        GSM result = Map.map("f @ {\"hello\"}", "f", "f @  (select({{\"bab\", {1, 2, 3}}} : x -> {(o.\"id\") == 3})) @  (select({{\"sis\", {8, 10, 24}}} : x -> {(o.\"id\") == 2})) ", gsm);
        result.O.forEach(System.out::println);

    }

}