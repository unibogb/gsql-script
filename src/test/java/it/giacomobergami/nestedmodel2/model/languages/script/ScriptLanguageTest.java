package it.giacomobergami.nestedmodel2.model.languages.script;

import it.giacomobergami.nestedmodel2.model.logical.object.model.object.ObjectModel;
import it.giacomobergami.nestedmodel2.utils.JId;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ScriptLanguageTest extends TestCase {

    ObjectModel om;

    @Before
    public void setUp() throws Exception {
        om = new ObjectModel(null, new JId(2345));
        om._ell.add("ciao1");
        om._ell.add("ciao2");
        om._ell.add("ciao3");
        ArrayList<JId> al = new ArrayList<>();
        al.add(new JId(0));al.add(new JId(1));al.add(new JId(2));
        om._phi.put("elemento", al);
        ArrayList<JId> al2 = new ArrayList<>();
        al2.add(new JId(3));al2.add(new JId(4));al2.add(new JId(5));
        om._phi.put("elementu", al2);
        /*

         */
    }

    @Test
    public void testId() {
        Assert.assertTrue(
                new ScriptLanguage("o.\"id\"")
                        .evaluateWithContext(om)
                        .print(false)
                        .equals("2345"));
    }

    @Test
    public void testPrintableFunction() {
        Assert.assertTrue(
                new ScriptLanguage("f := x -> { x + 3 }; g := y -> { (f y)*y }; f")
                        .evaluateWithContext(om)
                        .print(false)
                        .equals("x -> {(x) + (3)}"));
    }

    @Test
    public void testInvokeFunction() {
        Assert.assertTrue(new ScriptLanguage("f := x -> { x + 3 }; (f 5)")
                .evaluateWithContext(om)
                .print(false)
                .equals("8"));
    }

    @Test
    public void testMultipleFunctionInvoke() {
        Assert.assertTrue(new ScriptLanguage("f := x -> { x + 3 }; g := y -> { (f y)*y }; (g 2) ")
                .evaluateWithContext(om)
                .print(false)
                .equals("10"));
    }

    @Test
    public void testInvokeFunctionObject() {
        Assert.assertTrue(new ScriptLanguage("f := x -> { ((x[0]) / (x[1])) +5 }; (f {100, 2}) ")
                .evaluateWithContext(om)
                .print(false)
                .equals("55"));
    }

    @Test
    public void testAccessObjectWithNumber() {
        // If I use the varphi notation, then it is considered as an array
        Assert.assertTrue(new ScriptLanguage("f[0]")
                .setFunctionF(om.varphi())
                .evaluate()
                .print(true)
                .equals("{\"elemento\", {0, 1, 2}}"));
    }

    @Test
    public void testAccessObjectWithString() {
        // If I use the varphi notation, then it is considered as an array
        Assert.assertTrue(new ScriptLanguage("(f[\"elemento\"])")
                .setFunctionF(om._phi).evaluate()
                .print()
                .equals("{0, 1, 2}"));
    }

    @Test
    public void testDomain() {
        // If I use the varphi notation, then it is considered as an array
        new ScriptLanguage("dom := g -> {map( g : x -> { x[0] } )}; (dom f)")
                .setFunctionF(om._phi).evaluate()
                .print();
    }

    @Test
    public void testMethod() {
        // If I use the varphi notation, then it is considered as an array
        Assert.assertTrue(new ScriptLanguage("(o.\"ell\")")
                .evaluateWithContext(om)
                .print(true)
                .equals("{\"ciao1\", \"ciao2\", \"ciao3\"}"));
    }

    @Test
    public void testStringLength() {
        Assert.assertTrue( new ScriptLanguage("0 + (o.\"ell\")")
                .evaluateWithContext(om)
                .print(false)
                .equals("3"));
    }

    @Test
    public void testStringGet() {
        Assert.assertTrue(new ScriptLanguage(" (o.\"phi\") [\"elemento\"]  ")
                .evaluateWithContext(om)
                .print(false)
                .equals("{0, 1, 2}"));

        Assert.assertTrue(new ScriptLanguage(" (o.\"phi\") [\"elementu\"]  ")
                .evaluateWithContext(om)
                .print(false)
                .equals("{3, 4, 5}"));
    }

    @Test
    public void testStringGet2() {
        new ScriptLanguage("( select( (o.varphi) : x -> { x[0] == \"elemento\"} ) )[0] [1] ")
                .evaluateWithContext(om)
                .print(false)
                .equals("{0, 1, 2}");

        new ScriptLanguage("( select( (o.varphi) : x -> { x[0] == \"jiju\"} ) ) ")
                .evaluateWithContext(om)
                .print(false)
                .equals("{}");
    }

    @Test
    public void testMap() {
        Assert.assertTrue(new ScriptLanguage("map( (o.\"phi\") [\"elemento\"] : x -> { \"g\" ++ (x * 2) } ) ")
                .evaluateWithContext(om)
                .print(false)
                .equals("{\"g0\", \"g2\", \"g4\"}"));
    }

    @Test
    public void testEnumerate() {
        Assert.assertTrue(new ScriptLanguage("f := x -> { if (x >= 10) then x else (x @ (f (x+1))) }; (f 2)")
                .evaluate()
                .print(false)
                .equals("{2, 3, 4, 5, 6, 7, 8, 9, 10}"));
    }

    @Test
    public void testAccumulate() {
        Assert.assertTrue(new ScriptLanguage("" +
                "acc := 0; " +
                "sgen := x -> { if (x >= 10) then x else (x @ (sgen (x+1))) }; " +
                "s := (sgen 1); " +
                "select( s : x -> { acc := (x + acc) ; ff } );" +
                "acc")
                .evaluate()
                .toString()
                .equals("55"));
    }

    @Test
    public void testFilter() {
        Assert.assertTrue(new ScriptLanguage("select( (o.\"phi\") [\"elemento\"] : x -> { ((((x / 2) * 2 + 1) == (x + 1)) || (x == 0)) } ) ")
                .evaluateWithContext(om)
                .print(false)
                .equals("{0, 2}"));
    }

    @Test
    public void testAppend() {
        Assert.assertTrue(new ScriptLanguage("f @ {\"ciao4\"} @ {\"ciao 5\"}")
                .setFunctionF(om.ell()).evaluate().print(false)
                .equals("{\"ciao1\", \"ciao2\", \"ciao3\", \"ciao4\", \"ciao 5\"}"));
        Assert.assertTrue(new ScriptLanguage("{f} @ {{\"ciao4\"} @ {\"ciao 5\"}}")
                .setFunctionF(om.ell()).evaluate().print(false)
                .equals("{{\"ciao1\", \"ciao2\", \"ciao3\"}, ({\"ciao4\"}) @ ({\"ciao 5\"})}"));
        Assert.assertTrue(new ScriptLanguage("{f} @ {{\"ciao4\"} @ {\"ciao 5\"}}")
                .setFunctionF(om.ell()).evaluate().print(false)
                .equals("{{\"ciao1\", \"ciao2\", \"ciao3\"}, ({\"ciao4\"}) @ ({\"ciao 5\"})}"));
    }


}