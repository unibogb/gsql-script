package it.giacomobergami.nestedmodel2.model.languages.GSQL;

import it.giacomobergami.nestedmodel2.model.languages.GSQL.objects.DisjointUnionObject;
import it.giacomobergami.nestedmodel2.model.logical.object.model.object.ObjectModel;
import it.giacomobergami.nestedmodel2.utils.JId;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;


public class DisjointUnionObjectTest extends TestCase {
    public DisjointUnionObjectTest() {
        super();
    }

    @Test
    public void testEmpty() {
        ObjectModel o = new ObjectModel(null, new JId(1));
        Assert.assertEquals(o, new DisjointUnionObject(null, new JId(1), o, o).copy());
    }

    @Test
    public void testElements() {
        ObjectModel left = new ObjectModel(null, new JId(1));
        left._ell.add("hello");
        left._xi.add(("hello"));
        left._phi.put(("s"), new ArrayList<>());

        ObjectModel right = new ObjectModel(null, new JId(1));
        right._ell.add("world");
        right._xi.add(("world"));
        ArrayList<JId> idlist = new ArrayList<>();
        idlist.add(new JId(505));
        right._phi.put(("s"), idlist);

        ObjectModel expect = new ObjectModel(null, new JId(1));
        expect._ell.add("hello");
        expect._ell.add("world");
        expect._xi.add(("hello"));
        expect._xi.add(("world"));
        expect._phi.put("[1, s]", new ArrayList<>());
        expect._phi.put("[2, s]", idlist);
        Assert.assertEquals(expect, new DisjointUnionObject(null, new JId(1), left, right).copy());
    }

}