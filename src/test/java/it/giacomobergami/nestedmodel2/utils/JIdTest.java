package it.giacomobergami.nestedmodel2.utils;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

public class JIdTest extends TestCase {
    public JIdTest() { super(); }

    @Test
    public void testDoveTailing() {
        Random rand = new Random();
        for (int i = 0; i < 100; i++) {
            JId left = new JId(rand.nextInt(100));
            JId right = new JId(rand.nextInt(100));
            JPair<JId, JId> undove = JId.dovetail(left, right).undovetail();
            Assert.assertTrue(left.toString() + " vs. " + undove.key +" -- "+right.toString() + " vs. " + undove.value, left.equals(undove.key));
            Assert.assertTrue(left.toString() + " vs. " + undove.key +" -- "+right.toString() + " vs. " + undove.value, right.equals(undove.value));
        }
    }

}